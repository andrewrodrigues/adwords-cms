/*Table structure for table `acos` */

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;

/*Data for the table `acos` */

insert  into `acos`(`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (1,NULL,NULL,NULL,'controllers',1,436),(2,1,NULL,NULL,'Blocos',2,5),(3,2,NULL,NULL,'index',3,4),(4,1,NULL,NULL,'Buscas',6,9),(5,4,NULL,NULL,'index',7,8),(6,1,NULL,NULL,'Home',10,13),(7,6,NULL,NULL,'index',11,12),(8,1,NULL,NULL,'NoticiaTags',14,17),(9,8,NULL,NULL,'index',15,16),(10,1,NULL,NULL,'NoticiaTipos',18,21),(11,10,NULL,NULL,'index',19,20),(12,1,NULL,NULL,'Noticias',22,27),(13,12,NULL,NULL,'index',23,24),(14,12,NULL,NULL,'detalhe',25,26),(15,1,NULL,NULL,'Pages',28,31),(16,15,NULL,NULL,'display',29,30),(17,1,NULL,NULL,'Paginas',32,35),(18,17,NULL,NULL,'detalhe',33,34),(19,1,NULL,NULL,'Sac',36,39),(20,19,NULL,NULL,'add',37,38),(21,1,NULL,NULL,'Share',40,43),(22,21,NULL,NULL,'index',41,42),(23,1,NULL,NULL,'Sitemaps',44,47),(24,23,NULL,NULL,'index',45,46),(25,1,NULL,NULL,'Acl',48,101),(26,25,NULL,NULL,'Acl',49,52),(27,26,NULL,NULL,'fatorcms_index',50,51),(28,25,NULL,NULL,'Acos',53,64),(29,28,NULL,NULL,'fatorcms_index',54,55),(30,28,NULL,NULL,'fatorcms_empty_acos',56,57),(31,28,NULL,NULL,'fatorcms_build_acl',58,59),(32,28,NULL,NULL,'fatorcms_prune_acos',60,61),(33,28,NULL,NULL,'fatorcms_synchronize',62,63),(34,25,NULL,NULL,'Aros',65,100),(35,34,NULL,NULL,'fatorcms_index',66,67),(36,34,NULL,NULL,'fatorcms_check',68,69),(37,34,NULL,NULL,'fatorcms_users',70,71),(38,34,NULL,NULL,'fatorcms_update_user_role',72,73),(39,34,NULL,NULL,'fatorcms_ajax_role_permissions',74,75),(40,34,NULL,NULL,'fatorcms_role_permissions',76,77),(41,34,NULL,NULL,'fatorcms_user_permissions',78,79),(42,34,NULL,NULL,'fatorcms_empty_permissions',80,81),(43,34,NULL,NULL,'fatorcms_clear_user_specific_permissions',82,83),(44,34,NULL,NULL,'fatorcms_grant_all_controllers',84,85),(45,34,NULL,NULL,'fatorcms_deny_all_controllers',86,87),(46,34,NULL,NULL,'fatorcms_get_role_controller_permission',88,89),(47,34,NULL,NULL,'fatorcms_grant_role_permission',90,91),(48,34,NULL,NULL,'fatorcms_deny_role_permission',92,93),(49,34,NULL,NULL,'fatorcms_get_user_controller_permission',94,95),(50,34,NULL,NULL,'fatorcms_grant_user_permission',96,97),(51,34,NULL,NULL,'fatorcms_deny_user_permission',98,99),(52,1,NULL,NULL,'FdBanners',102,127),(53,52,NULL,NULL,'FdBannerTipos',103,114),(54,53,NULL,NULL,'fatorcms_index',104,105),(55,53,NULL,NULL,'fatorcms_add',106,107),(56,53,NULL,NULL,'fatorcms_edit',108,109),(57,53,NULL,NULL,'fatorcms_delete',110,111),(58,53,NULL,NULL,'fatorcms_status',112,113),(59,52,NULL,NULL,'FdBanners',115,126),(60,59,NULL,NULL,'fatorcms_index',116,117),(61,59,NULL,NULL,'fatorcms_add',118,119),(62,59,NULL,NULL,'fatorcms_edit',120,121),(63,59,NULL,NULL,'fatorcms_delete',122,123),(64,59,NULL,NULL,'fatorcms_status',124,125),(65,1,NULL,NULL,'FdBlocos',128,153),(66,65,NULL,NULL,'FdBlocoTipos',129,140),(67,66,NULL,NULL,'fatorcms_index',130,131),(68,66,NULL,NULL,'fatorcms_add',132,133),(69,66,NULL,NULL,'fatorcms_edit',134,135),(70,66,NULL,NULL,'fatorcms_delete',136,137),(71,66,NULL,NULL,'fatorcms_status',138,139),(72,65,NULL,NULL,'FdBlocos',141,152),(73,72,NULL,NULL,'fatorcms_index',142,143),(74,72,NULL,NULL,'fatorcms_add',144,145),(75,72,NULL,NULL,'fatorcms_edit',146,147),(76,72,NULL,NULL,'fatorcms_delete',148,149),(77,72,NULL,NULL,'fatorcms_status',150,151),(78,1,NULL,NULL,'FdDashboard',154,159),(79,78,NULL,NULL,'FdDashboard',155,158),(80,79,NULL,NULL,'fatorcms_index',156,157),(81,1,NULL,NULL,'FdEmails',160,177),(82,81,NULL,NULL,'FdEmails',161,166),(83,82,NULL,NULL,'fatorcms_index',162,163),(84,82,NULL,NULL,'fatorcms_view',164,165),(85,81,NULL,NULL,'FdTemplates',167,176),(86,85,NULL,NULL,'fatorcms_index',168,169),(87,85,NULL,NULL,'fatorcms_add',170,171),(88,85,NULL,NULL,'fatorcms_edit',172,173),(89,85,NULL,NULL,'fatorcms_delete',174,175),(90,1,NULL,NULL,'FdGalerias',178,209),(91,90,NULL,NULL,'FdGaleriaTipos',179,190),(92,91,NULL,NULL,'fatorcms_index',180,181),(93,91,NULL,NULL,'fatorcms_add',182,183),(94,91,NULL,NULL,'fatorcms_edit',184,185),(95,91,NULL,NULL,'fatorcms_delete',186,187),(96,91,NULL,NULL,'fatorcms_status',188,189),(97,90,NULL,NULL,'FdGalerias',191,208),(98,97,NULL,NULL,'fatorcms_index',192,193),(99,97,NULL,NULL,'fatorcms_add',194,195),(100,97,NULL,NULL,'fatorcms_edit',196,197),(101,97,NULL,NULL,'fatorcms_delete',198,199),(102,97,NULL,NULL,'fatorcms_images_save',200,201),(103,97,NULL,NULL,'fatorcms_images',202,203),(104,97,NULL,NULL,'fatorcms_upload',204,205),(105,97,NULL,NULL,'fatorcms_status',206,207),(106,1,NULL,NULL,'FdLogs',210,221),(107,106,NULL,NULL,'FdLogs',211,220),(108,107,NULL,NULL,'fatorcms_index',212,213),(109,107,NULL,NULL,'fatorcms_view',214,215),(110,107,NULL,NULL,'fatorcms_delete',216,217),(111,107,NULL,NULL,'fatorcms_status',218,219),(112,1,NULL,NULL,'FdMenus',222,249),(113,112,NULL,NULL,'FdLinks',223,238),(114,113,NULL,NULL,'fatorcms_index',224,225),(115,113,NULL,NULL,'fatorcms_add',226,227),(116,113,NULL,NULL,'fatorcms_edit',228,229),(117,113,NULL,NULL,'fatorcms_delete',230,231),(118,113,NULL,NULL,'fatorcms_moveup',232,233),(119,113,NULL,NULL,'fatorcms_movedown',234,235),(120,113,NULL,NULL,'fatorcms_status',236,237),(121,112,NULL,NULL,'FdMenus',239,248),(122,121,NULL,NULL,'fatorcms_index',240,241),(123,121,NULL,NULL,'fatorcms_add',242,243),(124,121,NULL,NULL,'fatorcms_edit',244,245),(125,121,NULL,NULL,'fatorcms_delete',246,247),(126,1,NULL,NULL,'FdNoticias',250,299),(127,126,NULL,NULL,'FdNoticiaCategorias',251,262),(128,127,NULL,NULL,'fatorcms_index',252,253),(129,127,NULL,NULL,'fatorcms_add',254,255),(130,127,NULL,NULL,'fatorcms_edit',256,257),(131,127,NULL,NULL,'fatorcms_delete',258,259),(132,127,NULL,NULL,'fatorcms_status',260,261),(133,126,NULL,NULL,'FdNoticiaTags',263,274),(134,133,NULL,NULL,'fatorcms_index',264,265),(135,133,NULL,NULL,'fatorcms_add',266,267),(136,133,NULL,NULL,'fatorcms_edit',268,269),(137,133,NULL,NULL,'fatorcms_delete',270,271),(138,133,NULL,NULL,'fatorcms_status',272,273),(139,126,NULL,NULL,'FdNoticiaTipos',275,286),(140,139,NULL,NULL,'fatorcms_index',276,277),(141,139,NULL,NULL,'fatorcms_add',278,279),(142,139,NULL,NULL,'fatorcms_edit',280,281),(143,139,NULL,NULL,'fatorcms_delete',282,283),(144,139,NULL,NULL,'fatorcms_status',284,285),(145,126,NULL,NULL,'FdNoticias',287,298),(146,145,NULL,NULL,'fatorcms_index',288,289),(147,145,NULL,NULL,'fatorcms_add',290,291),(148,145,NULL,NULL,'fatorcms_edit',292,293),(149,145,NULL,NULL,'fatorcms_delete',294,295),(150,145,NULL,NULL,'fatorcms_status',296,297),(151,1,NULL,NULL,'FdPaginas',300,325),(152,151,NULL,NULL,'FdPaginaTipos',301,312),(153,152,NULL,NULL,'fatorcms_index',302,303),(154,152,NULL,NULL,'fatorcms_add',304,305),(155,152,NULL,NULL,'fatorcms_edit',306,307),(156,152,NULL,NULL,'fatorcms_delete',308,309),(157,152,NULL,NULL,'fatorcms_status',310,311),(158,151,NULL,NULL,'FdPaginas',313,324),(159,158,NULL,NULL,'fatorcms_index',314,315),(160,158,NULL,NULL,'fatorcms_add',316,317),(161,158,NULL,NULL,'fatorcms_edit',318,319),(162,158,NULL,NULL,'fatorcms_delete',320,321),(163,158,NULL,NULL,'fatorcms_status',322,323),(164,1,NULL,NULL,'FdRotas',326,347),(165,164,NULL,NULL,'FdRedirecionamentos',327,336),(166,165,NULL,NULL,'fatorcms_index',328,329),(167,165,NULL,NULL,'fatorcms_add',330,331),(168,165,NULL,NULL,'fatorcms_edit',332,333),(169,165,NULL,NULL,'fatorcms_delete',334,335),(170,164,NULL,NULL,'FdRotas',337,346),(171,170,NULL,NULL,'fatorcms_index',338,339),(172,170,NULL,NULL,'fatorcms_add',340,341),(173,170,NULL,NULL,'fatorcms_edit',342,343),(174,170,NULL,NULL,'fatorcms_delete',344,345),(175,1,NULL,NULL,'FdSac',348,373),(176,175,NULL,NULL,'FdFuncionarios',349,352),(177,176,NULL,NULL,'fatorcms_index',350,351),(178,175,NULL,NULL,'FdSac',353,360),(179,178,NULL,NULL,'fatorcms_index',354,355),(180,178,NULL,NULL,'fatorcms_view',356,357),(181,178,NULL,NULL,'fatorcms_delete',358,359),(182,175,NULL,NULL,'FdSacTipos',361,372),(183,182,NULL,NULL,'fatorcms_index',362,363),(184,182,NULL,NULL,'fatorcms_add',364,365),(185,182,NULL,NULL,'fatorcms_edit',366,367),(186,182,NULL,NULL,'fatorcms_delete',368,369),(187,182,NULL,NULL,'fatorcms_status',370,371),(188,1,NULL,NULL,'FdSettings',374,389),(189,188,NULL,NULL,'FdSettings',375,388),(190,189,NULL,NULL,'fatorcms_index',376,377),(191,189,NULL,NULL,'fatorcms_add',378,379),(192,189,NULL,NULL,'fatorcms_edit',380,381),(193,189,NULL,NULL,'fatorcms_delete',382,383),(194,189,NULL,NULL,'fatorcms_prefix',384,385),(195,189,NULL,NULL,'fatorcms_reset',386,387),(196,1,NULL,NULL,'FdSvn',390,401),(197,196,NULL,NULL,'FdSvn',391,400),(198,197,NULL,NULL,'fatorcms_index',392,393),(199,197,NULL,NULL,'fatorcms_view',394,395),(200,197,NULL,NULL,'fatorcms_delete',396,397),(201,197,NULL,NULL,'fatorcms_status',398,399),(202,1,NULL,NULL,'FdUsuarios',402,435),(203,202,NULL,NULL,'FdGrupos',403,414),(204,203,NULL,NULL,'fatorcms_index',404,405),(205,203,NULL,NULL,'fatorcms_add',406,407),(206,203,NULL,NULL,'fatorcms_edit',408,409),(207,203,NULL,NULL,'fatorcms_delete',410,411),(208,203,NULL,NULL,'fatorcms_status',412,413),(209,202,NULL,NULL,'FdUsuarios',415,434),(210,209,NULL,NULL,'fatorcms_login',416,417),(211,209,NULL,NULL,'fatorcms_logout',418,419),(212,209,NULL,NULL,'fatorcms_index',420,421),(213,209,NULL,NULL,'fatorcms_add',422,423),(214,209,NULL,NULL,'fatorcms_edit',424,425),(215,209,NULL,NULL,'fatorcms_update_pass',426,427),(216,209,NULL,NULL,'fatorcms_delete',428,429),(217,209,NULL,NULL,'fatorcms_status',430,431),(218,209,NULL,NULL,'sendPassword',432,433);

/*Table structure for table `aros` */

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `aros` */

insert  into `aros`(`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (1,NULL,'Grupo',1,'',1,4),(2,NULL,'Grupo',2,'',5,6),(3,NULL,'Grupo',3,'',7,8),(4,NULL,'Grupo',4,'',9,10),(5,1,'Usuario',1,'',2,3);

/*Table structure for table `aros_acos` */

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) unsigned NOT NULL,
  `aco_id` int(10) unsigned NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8;

/*Data for the table `aros_acos` */

insert  into `aros_acos`(`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (1,1,1,'1','1','1','1'),(2,4,24,'1','1','1','1'),(3,4,22,'1','1','1','1'),(4,4,20,'1','1','1','1'),(5,4,18,'1','1','1','1'),(6,4,16,'1','1','1','1'),(7,4,13,'1','1','1','1'),(8,4,14,'1','1','1','1'),(9,4,11,'1','1','1','1'),(10,4,9,'1','1','1','1'),(11,4,7,'1','1','1','1'),(12,4,5,'1','1','1','1'),(13,4,3,'1','1','1','1'),(14,3,3,'1','1','1','1'),(15,2,3,'1','1','1','1'),(16,2,5,'1','1','1','1'),(17,3,5,'1','1','1','1'),(18,3,7,'1','1','1','1'),(19,2,7,'1','1','1','1'),(20,2,9,'1','1','1','1'),(21,3,9,'1','1','1','1'),(22,3,11,'1','1','1','1'),(23,2,11,'1','1','1','1'),(24,3,14,'1','1','1','1'),(25,2,14,'1','1','1','1'),(26,2,13,'1','1','1','1'),(27,3,13,'1','1','1','1'),(28,3,16,'1','1','1','1'),(29,2,16,'1','1','1','1'),(30,2,18,'1','1','1','1'),(31,2,20,'1','1','1','1'),(32,2,22,'1','1','1','1'),(33,2,24,'1','1','1','1'),(34,3,24,'1','1','1','1'),(35,3,22,'1','1','1','1'),(36,3,20,'1','1','1','1'),(37,3,18,'1','1','1','1'),(43,2,64,'1','1','1','1'),(44,2,60,'1','1','1','1'),(45,2,62,'1','1','1','1'),(46,2,63,'1','1','1','1'),(47,2,61,'1','1','1','1'),(48,2,73,'1','1','1','1'),(49,2,77,'1','1','1','1'),(50,2,75,'1','1','1','1'),(51,2,76,'1','1','1','1'),(52,2,74,'1','1','1','1'),(53,2,80,'1','1','1','1'),(54,3,80,'1','1','1','1'),(55,2,83,'1','1','1','1'),(56,2,84,'1','1','1','1'),(57,2,86,'1','1','1','1'),(58,2,88,'1','1','1','1'),(59,2,87,'1','1','1','1'),(60,2,99,'1','1','1','1'),(61,2,101,'1','1','1','1'),(62,2,100,'1','1','1','1'),(63,2,103,'1','1','1','1'),(64,2,102,'1','1','1','1'),(65,2,98,'1','1','1','1'),(66,2,105,'1','1','1','1'),(67,2,104,'1','1','1','1'),(68,2,108,'1','1','1','1'),(69,2,111,'1','1','1','1'),(70,2,109,'1','1','1','1'),(71,2,115,'1','1','1','1'),(73,2,116,'1','1','1','1'),(75,2,114,'1','1','1','1'),(76,2,119,'1','1','1','1'),(77,2,118,'1','1','1','1'),(78,2,120,'1','1','1','1'),(79,2,123,'1','1','1','1'),(81,2,122,'1','1','1','1'),(82,2,124,'1','1','1','1'),(83,2,117,'1','1','1','1'),(84,2,129,'1','1','1','1'),(85,2,131,'1','1','1','1'),(86,2,130,'1','1','1','1'),(87,2,128,'1','1','1','1'),(88,2,132,'1','1','1','1'),(89,2,135,'1','1','1','1'),(90,2,137,'1','1','1','1'),(91,2,136,'1','1','1','1'),(92,2,134,'1','1','1','1'),(93,2,138,'1','1','1','1'),(94,2,141,'1','1','1','1'),(95,2,143,'1','1','1','1'),(96,2,142,'1','1','1','1'),(97,2,150,'1','1','1','1'),(98,2,146,'1','1','1','1'),(99,2,148,'1','1','1','1'),(100,2,149,'1','1','1','1'),(101,2,147,'1','1','1','1'),(102,2,144,'1','1','1','1'),(103,2,140,'1','1','1','1'),(104,2,163,'1','1','1','1'),(105,2,159,'1','1','1','1'),(106,2,161,'1','1','1','1'),(107,2,162,'1','1','1','1'),(108,2,157,'1','1','1','1'),(109,2,160,'1','1','1','1'),(110,2,155,'1','1','1','1'),(111,2,156,'1','1','1','1'),(112,2,154,'1','1','1','1'),(113,2,153,'1','1','1','1'),(114,2,167,'1','1','1','1'),(115,2,169,'1','1','1','1'),(116,2,168,'1','1','1','1'),(117,2,166,'1','1','1','1'),(118,2,172,'1','1','1','1'),(119,2,173,'1','1','1','1'),(120,2,171,'1','1','1','1'),(121,2,187,'1','1','1','1'),(122,2,183,'1','1','1','1'),(123,2,185,'1','1','1','1'),(124,2,184,'1','1','1','1'),(125,2,180,'1','1','1','1'),(126,2,179,'1','1','1','1'),(127,2,181,'1','1','1','1'),(128,2,177,'1','1','1','1'),(129,2,191,'1','1','1','1'),(130,2,192,'1','1','1','1'),(131,2,190,'1','1','1','1'),(132,2,194,'1','1','1','1'),(133,2,195,'1','1','1','1'),(134,2,198,'1','1','1','1'),(135,2,201,'1','1','1','1'),(136,2,199,'1','1','1','1'),(137,2,205,'1','1','1','1'),(138,2,206,'1','1','1','1'),(139,2,204,'1','1','1','1'),(140,2,208,'1','1','1','1'),(141,2,213,'1','1','1','1'),(142,2,216,'1','1','1','1'),(143,2,214,'1','1','1','1'),(144,2,212,'1','1','1','1'),(145,2,210,'1','1','1','1'),(146,2,218,'1','1','1','1'),(147,2,215,'1','1','1','1'),(148,2,217,'1','1','1','1'),(149,2,211,'1','1','1','1');

/*Table structure for table `banner_tipos` */

DROP TABLE IF EXISTS `banner_tipos`;

CREATE TABLE `banner_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET latin1 NOT NULL,
  `codigo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` tinyint(4) NOT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `banner_tipos` */

insert  into `banner_tipos`(`id`,`nome`,`codigo`,`status`,`usuario_id`,`created`,`modified`) values (1,'Página Inicial','site_topo',1,1,'2013-11-25 23:37:11','2015-08-11 15:03:18'),(2,'Páginas','site_pagina',1,1,'2015-08-11 15:03:59','2015-08-11 15:03:59'),(3,'Notícias','site_noticia',1,1,'2015-08-11 15:04:09','2015-08-11 15:04:09'),(4,'Sidebar','site_sidebar',1,1,'2015-08-18 21:57:07','2015-08-19 01:30:08'),(6,'Floating','site_floating',1,1,'2016-01-26 18:50:30','2016-01-26 18:50:30');

/*Table structure for table `banners` */

DROP TABLE IF EXISTS `banners`;

CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sites` text,
  `banner_tipo_id` int(11) NOT NULL,
  `visivel` enum('GERAL','ALUNO','PROSPECT') DEFAULT NULL,
  `nome` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `file` varchar(255) CHARACTER SET latin1 NOT NULL,
  `dir` varchar(255) CHARACTER SET latin1 NOT NULL,
  `type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `size` varchar(255) CHARACTER SET latin1 NOT NULL,
  `path` varchar(255) DEFAULT 'files/banner/file',
  `url_status` tinyint(4) DEFAULT '0',
  `url_tipo` varchar(1) DEFAULT NULL,
  `url` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `url_video` varchar(500) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `file_mobile` varchar(255) DEFAULT NULL,
  `dir_mobile` varchar(255) DEFAULT NULL,
  `type_mobile` varchar(255) DEFAULT NULL,
  `size_mobile` varchar(255) DEFAULT NULL,
  `path_mobile` varchar(255) DEFAULT 'files/banner/file_mobile',
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `data_inicio` datetime DEFAULT NULL,
  `data_fim` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `banners` */

/*Table structure for table `bloco_tipos` */

DROP TABLE IF EXISTS `bloco_tipos`;

CREATE TABLE `bloco_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `bloco_tipos` */

insert  into `bloco_tipos`(`id`,`nome`,`slug`,`status`,`created`,`modified`) values (1,'Notícias','noticias',1,'2015-08-25 12:35:41','2015-08-25 12:35:41'),(2,'Diversos','diversos',1,'2015-11-09 13:12:35','2015-11-09 13:12:35');

/*Table structure for table `blocos` */

DROP TABLE IF EXISTS `blocos`;

CREATE TABLE `blocos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sites` text,
  `bloco_tipo_id` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `conteudo` text,
  `seo_url` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` text,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `editavel` tinyint(4) DEFAULT '1',
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `blocos` */

/*Table structure for table `emails` */

DROP TABLE IF EXISTS `emails`;

CREATE TABLE `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to` text,
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  `status` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `emails` */

/*Table structure for table `galeria_imagens` */

DROP TABLE IF EXISTS `galeria_imagens`;

CREATE TABLE `galeria_imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `galeria_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `dir` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL DEFAULT 'files/galeria/file',
  `legenda` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `galeria_imagens` */

/*Table structure for table `galeria_tipos` */

DROP TABLE IF EXISTS `galeria_tipos`;

CREATE TABLE `galeria_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` tinyint(4) NOT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `galeria_tipos` */

/*Table structure for table `galerias` */

DROP TABLE IF EXISTS `galerias`;

CREATE TABLE `galerias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `galeria_tipo_id` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `id_tmp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `galerias` */

/*Table structure for table `grupos` */

DROP TABLE IF EXISTS `grupos`;

CREATE TABLE `grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `u_usuario_id` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `grupos` */

insert  into `grupos`(`id`,`nome`,`status`,`ordem`,`usuario_id`,`created`,`modified`) values (1,'Super Administrador',1,NULL,1,'2016-03-14 18:54:43','2016-03-14 18:54:43'),(2,'Administrador',1,NULL,1,'2016-03-14 18:54:51','2016-03-14 18:54:51'),(3,'Gerente',1,NULL,1,'2016-03-14 18:55:01','2016-03-14 18:55:01'),(4,'Público',1,NULL,1,'2016-03-14 18:55:10','2016-03-14 18:55:10');

/*Table structure for table `links` */

DROP TABLE IF EXISTS `links`;

CREATE TABLE `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `seo_url` varchar(255) DEFAULT NULL,
  `target` varchar(255) NOT NULL DEFAULT 'self',
  `rel` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `visibility_grupos` text,
  `tipo` enum('NORMAL','PAGINA','CATEGORIA') DEFAULT NULL,
  `pagina_id` int(11) DEFAULT NULL,
  `thumb_file` varchar(255) DEFAULT NULL,
  `thumb_dir` varchar(255) DEFAULT NULL,
  `thumb_type` varchar(255) DEFAULT NULL,
  `thumb_size` varchar(255) DEFAULT NULL,
  `thumb_path` varchar(255) DEFAULT 'files/link/thumb_file',
  `class` varchar(255) DEFAULT NULL,
  `class_estrutura` varchar(255) DEFAULT NULL,
  `class_icon` varchar(255) DEFAULT NULL COMMENT 'class da icone da campus',
  `status` tinyint(4) NOT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_1` (`menu_id`,`status`,`visibility_grupos`(100))
) ENGINE=MyISAM AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

/*Data for the table `links` */

insert  into `links`(`id`,`menu_id`,`title`,`description`,`seo_url`,`target`,`rel`,`parent_id`,`lft`,`rght`,`visibility_grupos`,`tipo`,`pagina_id`,`thumb_file`,`thumb_dir`,`thumb_type`,`thumb_size`,`thumb_path`,`class`,`class_estrutura`,`class_icon`,`status`,`usuario_id`,`created`,`modified`) values (1,1,'Dashboard',NULL,'fatorcms','_self',NULL,NULL,1,2,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file','fa-dashboard',NULL,NULL,1,1,'2015-01-07 16:58:19','2016-02-25 21:47:36'),(2,1,'Usuários e Grupos',NULL,'#','_self',NULL,NULL,3,10,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file','fa-group',NULL,NULL,1,1,'2015-01-07 17:05:04','2015-01-29 18:50:27'),(3,1,'Usuários',NULL,'fatorcms/usuarios','_self',NULL,2,4,5,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-07 17:06:28','2015-01-29 18:53:17'),(4,1,'Grupos',NULL,'fatorcms/grupos','_self',NULL,2,6,7,'[\"1\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-07 17:06:43','2015-08-21 13:58:16'),(5,1,'Controle de Acessos',NULL,'fatorcms/acl','_self',NULL,2,8,9,'[\"1\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-07 17:07:07','2015-08-21 13:58:02'),(6,1,'Sistema',NULL,'#','_self',NULL,NULL,35,60,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file','fa-gears',NULL,NULL,1,1,'2015-01-07 17:09:30','2015-01-29 18:51:30'),(7,1,'Menus',NULL,'fatorcms/menus','_self',NULL,12,18,19,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-07 17:12:13','2015-08-18 10:56:17'),(8,1,'Redirecionamentos',NULL,'fatorcms/redirecionamentos','_self',NULL,6,36,37,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,NULL,'2015-01-07 17:12:43','2015-01-08 17:12:38'),(9,1,'Rotas',NULL,'fatorcms/rotas','_self',NULL,6,38,39,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-07 17:13:04','2015-12-21 21:45:31'),(10,1,'Configurações',NULL,'fatorcms/settings','_self',NULL,6,40,41,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,NULL,'2015-01-08 14:59:52','2015-01-08 14:59:52'),(11,1,'Templates de E-mail',NULL,'fatorcms/templates','_self',NULL,6,42,43,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,NULL,'2015-01-08 16:25:46','2015-01-08 17:13:01'),(12,1,'Site',NULL,'#','_self',NULL,NULL,11,20,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file','fa-desktop',NULL,NULL,1,1,'2015-01-08 18:07:08','2015-08-25 12:24:03'),(13,1,'Páginas',NULL,'fatorcms/paginas','_self',NULL,12,12,13,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-08 18:10:51','2015-01-29 18:54:18'),(14,1,'Banners',NULL,'fatorcms/banners','_self',NULL,12,14,15,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-09 16:06:18','2015-01-29 18:54:34'),(15,1,'Tipos de Banners',NULL,'fatorcms/banner_tipos','_self',NULL,100,51,52,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-12 10:53:54','2015-08-03 15:08:30'),(17,1,'SAC',NULL,'fatorcms/sac','_self',NULL,NULL,33,34,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file','fa-comments-o',NULL,NULL,1,1,'2015-01-12 13:31:16','2016-02-25 21:48:05'),(18,1,'Tipos de Sacs',NULL,'fatorcms/sac_tipos','_self',NULL,100,49,50,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-12 14:37:08','2015-08-03 15:08:25'),(19,1,'Log de Banco',NULL,'fatorcms/logs','_self',NULL,6,44,45,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-12 16:08:09','2015-01-29 18:59:15'),(26,1,'E-mails',NULL,'fatorcms/emails','_self',NULL,6,46,47,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-21 12:06:04','2015-01-29 18:59:23'),(27,1,'Blocos',NULL,'fatorcms/blocos','_self',NULL,12,16,17,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-01-28 21:03:38','2015-08-25 12:23:50'),(28,1,'SVN',NULL,'fatorcms/svn','_self',NULL,6,58,59,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-06-22 23:48:33','2015-06-22 23:49:30'),(29,1,'Notícias',NULL,'#','_self',NULL,NULL,21,30,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file','fa-book',NULL,NULL,1,1,'2015-06-26 01:46:10','2015-07-24 00:01:08'),(31,1,'Tipos',NULL,'fatorcms/noticia_tipos','_self',NULL,29,26,27,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-06-26 01:49:01','2015-07-24 00:02:44'),(32,1,'Tags',NULL,'fatorcms/noticia_tags','_self',NULL,29,24,25,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-06-26 02:25:57','2015-06-26 22:00:56'),(33,2,'Centro Universitário',NULL,'o-centro-universitario','_self',NULL,NULL,1,2,'[\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-07-10 13:03:03','2015-07-15 00:11:09'),(34,2,'Reitoria',NULL,'reitoria','_self',NULL,NULL,3,4,'[\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-07-10 13:03:36','2015-07-15 02:34:28'),(35,2,'Prograd',NULL,'prograd','_self',NULL,NULL,5,6,'[\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-07-10 13:03:55','2015-07-17 23:11:33'),(36,2,'Propex',NULL,'propex','_self',NULL,NULL,7,8,'[\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-07-10 13:04:11','2015-07-17 23:12:02'),(37,2,'Rede Laureate',NULL,'rede-laureate','_self',NULL,NULL,9,10,'[\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-07-10 13:04:44','2015-07-17 23:12:31'),(38,2,'Nossos Campi',NULL,'nossos-campi','_self',NULL,NULL,11,12,'[\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-07-10 13:05:00','2015-08-21 02:07:49'),(39,2,'Avaliação Institucional',NULL,'avaliacao-institucional','_self',NULL,NULL,13,14,'[\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-07-10 13:05:17','2015-07-17 23:13:29'),(86,1,'Tipos de Páginas',NULL,'fatorcms/pagina_tipos','_self',NULL,100,53,54,'[\"1\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-07-14 00:53:30','2015-08-03 15:08:40'),(90,1,'Categorias',NULL,'fatorcms/noticia_categorias','_self',NULL,29,28,29,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-07-20 15:24:08','2016-03-14 20:14:11'),(91,1,'Notícias',NULL,'fatorcms/noticias','_self',NULL,29,22,23,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-07-24 21:23:33','2016-03-14 20:11:40'),(99,1,'Tipos de Galeria',NULL,'fatorcms/galeria_tipos','_self',NULL,100,55,56,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-08-03 15:01:29','2015-08-03 15:08:47'),(98,1,'Galerias',NULL,'fatorcms/galerias','_self',NULL,NULL,31,32,'[\"1\",\"2\",\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file','fa-picture-o',NULL,NULL,1,1,'2015-08-03 15:00:12','2015-08-03 15:00:56'),(100,1,'Tipos',NULL,'#','_self',NULL,6,48,57,'[\"1\",\"2\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-08-03 15:07:47','2015-08-03 15:07:47'),(102,2,'Convênios e parcerias',NULL,'convenios-e-parcerias','_self',NULL,NULL,15,16,'[\"3\"]',NULL,NULL,NULL,NULL,NULL,NULL,'files/link/thumb_file',NULL,NULL,NULL,1,1,'2015-08-21 02:51:49','2015-08-21 02:51:49');

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` mediumtext NOT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `acao` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `logs` */

/*Table structure for table `menus` */

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `descricao` text,
  `status` tinyint(4) NOT NULL,
  `link_count` int(11) DEFAULT NULL,
  `params` text,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `menus` */

insert  into `menus`(`id`,`nome`,`slug`,`descricao`,`status`,`link_count`,`params`,`usuario_id`,`created`,`modified`) values (1,'Admin','admin','Admin da CMS',1,32,NULL,NULL,'2014-04-06 21:15:45','2015-01-07 09:19:24'),(2,'Topo','site_institucional','Menu institucional do portal.',1,8,NULL,1,'2015-07-10 12:54:22','2015-08-21 14:13:43');

/*Table structure for table `noticia_blocos` */

DROP TABLE IF EXISTS `noticia_blocos`;

CREATE TABLE `noticia_blocos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bloco_id` int(11) NOT NULL,
  `noticia_id` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `status_old` tinyint(4) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `noticia_blocos` */

/*Table structure for table `noticia_categorias` */

DROP TABLE IF EXISTS `noticia_categorias`;

CREATE TABLE `noticia_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noticia_tipo_id` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `seo_url` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(500) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `class` varchar(45) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `noticia_categorias` */

/*Table structure for table `noticia_tags` */

DROP TABLE IF EXISTS `noticia_tags`;

CREATE TABLE `noticia_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `seo_url` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `noticia_tags` */

/*Table structure for table `noticia_tipos` */

DROP TABLE IF EXISTS `noticia_tipos`;

CREATE TABLE `noticia_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `apresentacao` text,
  `seo_url` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(500) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `formulario` tinyint(4) DEFAULT NULL,
  `formulario_email` text,
  `formulario_titulo` varchar(255) DEFAULT NULL,
  `slogan` text,
  `conceitual` varchar(255) DEFAULT NULL,
  `conceitual_dir` varchar(255) DEFAULT NULL,
  `conceitual_type` varchar(255) DEFAULT NULL,
  `conceitual_size` varchar(255) DEFAULT NULL,
  `conceitual_path` varchar(255) DEFAULT 'files/noticia_tipo/conceitual',
  `class` varchar(45) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `noticia_tipos` */

insert  into `noticia_tipos`(`id`,`nome`,`apresentacao`,`seo_url`,`seo_title`,`seo_description`,`seo_keywords`,`formulario`,`formulario_email`,`formulario_titulo`,`slogan`,`conceitual`,`conceitual_dir`,`conceitual_type`,`conceitual_size`,`conceitual_path`,`class`,`status`,`created`,`modified`,`ordem`) values (1,'Notícias',NULL,'noticias','Notícias','','',1,'nra@uniritter.edu.br','',NULL,NULL,NULL,NULL,NULL,'files/noticia_tipo/conceitual',NULL,1,'2015-07-23 00:27:23','2015-12-03 13:37:40',NULL);

/*Table structure for table `noticias` */

DROP TABLE IF EXISTS `noticias`;

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noticia_tipo_id` int(11) NOT NULL,
  `noticia_categoria_id` int(11) DEFAULT NULL,
  `visivel` enum('GERAL','ALUNO','PROSPECT') DEFAULT NULL,
  `titulo` varchar(255) NOT NULL,
  `conteudo` text NOT NULL,
  `conteudo_rascunho` text,
  `descricao_resumida` varchar(255) DEFAULT NULL,
  `data_publicacao` date DEFAULT NULL,
  `destaque` tinyint(4) DEFAULT '0',
  `galeria_id` int(11) DEFAULT NULL,
  `seo_url` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` text,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `thumb_dir` varchar(255) DEFAULT NULL,
  `thumb_size` varchar(255) DEFAULT NULL,
  `thumb_type` varchar(255) DEFAULT NULL,
  `thumb_path` varchar(255) DEFAULT 'files/noticia/thumb',
  `thumb_legenda` varchar(255) DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `banner_dir` varchar(255) DEFAULT NULL,
  `banner_size` varchar(255) DEFAULT NULL,
  `banner_type` varchar(255) DEFAULT NULL,
  `banner_path` varchar(255) DEFAULT 'files/noticia/banner',
  `banner_alone` tinyint(4) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `views_week` int(11) DEFAULT NULL,
  `views_week_number` int(11) DEFAULT NULL,
  `views_mouth` int(11) DEFAULT NULL,
  `views_current_month` int(11) DEFAULT NULL,
  `formulario` tinyint(4) DEFAULT NULL,
  `formulario_email` varchar(255) DEFAULT NULL,
  `formulario_titulo` varchar(255) DEFAULT NULL,
  `id_old` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `noticia_tipo_id` (`noticia_tipo_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `noticias` */

/*Table structure for table `pagina_tipos` */

DROP TABLE IF EXISTS `pagina_tipos`;

CREATE TABLE `pagina_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `seo_url` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(500) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `class` varchar(45) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `pagina_tipos` */

insert  into `pagina_tipos`(`id`,`nome`,`seo_url`,`seo_title`,`seo_description`,`seo_keywords`,`class`,`status`,`created`,`modified`) values (1,'Institucional','institucional','Institucional','','',NULL,1,'2015-07-14 01:09:44','2015-07-14 01:09:44');

/*Table structure for table `paginas` */

DROP TABLE IF EXISTS `paginas`;

CREATE TABLE `paginas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sites` text,
  `pagina_tipo_id` int(11) DEFAULT NULL,
  `curso_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `conteudo` longtext,
  `conteudo_rascunho` text,
  `alerta` tinyint(4) DEFAULT NULL,
  `dinamico` tinyint(4) DEFAULT '0',
  `dinamico_elemento` varchar(255) DEFAULT NULL,
  `tem_sidebar` tinyint(4) DEFAULT NULL,
  `sidebar` text,
  `sidebar_status` tinyint(4) DEFAULT '1',
  `pagina_elemento_id` int(4) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `banner_dir` varchar(255) DEFAULT NULL,
  `banner_size` varchar(255) DEFAULT NULL,
  `banner_type` varchar(255) DEFAULT NULL,
  `banner_path` varchar(255) DEFAULT 'files/pagina/banner',
  `banner_alone` tinyint(4) DEFAULT NULL,
  `formulario` tinyint(4) DEFAULT NULL,
  `formulario_email` varchar(255) DEFAULT NULL,
  `formulario_titulo` varchar(255) DEFAULT NULL,
  `botao_aba_status` tinyint(4) DEFAULT '0',
  `botao_aba_url` text,
  `botao_aba_label` varchar(255) DEFAULT NULL,
  `botao_aba_target` varchar(255) DEFAULT '_self',
  `bloco_noticia_status` tinyint(4) DEFAULT NULL,
  `bloco_noticia_id` int(11) DEFAULT NULL,
  `bloco_noticia_slug` varchar(255) DEFAULT NULL,
  `seo_url` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `aba_ordem` int(11) DEFAULT '0',
  `botao_aba_2_status` tinyint(4) DEFAULT '0',
  `botao_aba_2_url` text,
  `botao_aba_2_label` varchar(255) DEFAULT NULL,
  `botao_aba_2_target` varchar(255) DEFAULT '_self',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `paginas` */

/*Table structure for table `redirecionamentos` */

DROP TABLE IF EXISTS `redirecionamentos`;

CREATE TABLE `redirecionamentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_antiga` text,
  `url_nova` text,
  `tipo` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `redirecionamentos` */

/*Table structure for table `rotas` */

DROP TABLE IF EXISTS `rotas`;

CREATE TABLE `rotas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sites` text COMMENT 'flag de portal/hotsite',
  `seo_url` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `row_id` int(11) DEFAULT NULL,
  `params_id` varchar(255) DEFAULT NULL,
  `params_value` varchar(255) DEFAULT NULL,
  `nome` text,
  `conteudo` mediumtext,
  `data` date DEFAULT NULL,
  `seo_keywords` text,
  `seo_description` text,
  `buscavel` tinyint(4) DEFAULT '0',
  `buscavel_ordem` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `nome` (`nome`,`conteudo`,`seo_keywords`),
  FULLTEXT KEY `nome_2` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `rotas` */

/*Table structure for table `sac` */

DROP TABLE IF EXISTS `sac`;

CREATE TABLE `sac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sac_tipo_id` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `mensagem` text,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `observacao` text,
  `url` text,
  `lead_tipo` enum('aluno','professor','funcionario','publico') DEFAULT NULL,
  `curso` varchar(255) DEFAULT NULL,
  `semestre` varchar(255) DEFAULT NULL,
  `data_nascimento` varchar(255) DEFAULT NULL,
  `celular` varchar(255) DEFAULT NULL,
  `intercambio` tinyint(4) DEFAULT NULL,
  `intercambio_local` varchar(500) DEFAULT NULL,
  `destinos` text,
  `destino_outro` varchar(500) DEFAULT NULL,
  `coci_id` varchar(255) DEFAULT NULL,
  `curs_id` varchar(255) DEFAULT NULL,
  `nens_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sac` */

/*Table structure for table `sac_tipos` */

DROP TABLE IF EXISTS `sac_tipos`;

CREATE TABLE `sac_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `sac_tipos` */

insert  into `sac_tipos`(`id`,`nome`,`slug`,`email`,`status`,`usuario_id`,`created`,`modified`) values (1,'Fale Conosco','fale_conosco','cleber.alves@fatordigital.com.br',1,1,'2013-08-18 21:54:19','2015-01-12 14:38:31');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `visivel` tinyint(4) DEFAULT '1',
  `class` varchar(255) DEFAULT NULL,
  `type` enum('boolean','text','textarea') DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

/*Data for the table `settings` */

insert  into `settings`(`id`,`key`,`value`,`status`,`visivel`,`class`,`type`,`usuario_id`,`created`,`modified`) values (1,'Site.Nome','Observatório da Segurança Cidadã',1,1,NULL,'text',NULL,'2013-08-21 00:09:26','2014-07-29 23:46:42'),(2,'Site.Seo.Title','UniRitter Laureate International Universities',1,1,NULL,'text',NULL,'2013-08-21 00:09:26','2015-01-08 16:16:54'),(3,'Site.Seo.Description','',1,1,NULL,'textarea',1,'2013-08-21 00:09:26','2015-11-27 22:28:55'),(4,'Site.Seo.Keywords','',1,1,NULL,'textarea',NULL,'2013-08-21 00:09:38','2014-07-29 23:48:47'),(5,'Site.Email.CC','cleber.alves@fatordigital.com.br',1,0,NULL,'text',NULL,'2014-01-13 20:06:45','2014-01-13 20:06:45'),(6,'Site.Social.Facebook','https://www.facebook.com/',1,1,NULL,'text',14,'2014-04-21 12:03:34','2015-08-24 14:52:45'),(7,'Site.Social.Twitter','https://twitter.com/',1,1,NULL,'text',14,'2014-04-21 12:03:34','2015-08-24 14:53:10'),(8,'Site.Social.Instagram','https://instagram.com/',1,1,NULL,'text',14,'2014-04-21 12:03:34','2015-08-24 14:53:29'),(9,'Site.Social.Youtube','https://www.youtube.com/user/',1,1,NULL,'text',14,'2014-04-21 12:03:34','2015-08-24 14:53:59'),(10,'Site.Google.Analytics','',1,1,NULL,'text',NULL,'2014-04-21 12:03:34','2014-04-21 12:03:34'),(62,'Site.Seo.Author','FatorDigital - www.fatordigital.com.br',1,0,NULL,'text',NULL,'2014-04-21 12:03:34','2014-04-21 12:03:34'),(63,'Site.Social.GooglePlus','https://plus.google.com/',1,1,NULL,'text',14,'2014-04-21 12:03:34','2015-08-24 14:55:23'),(64,'Site.SAC','',1,1,NULL,'text',NULL,'2014-04-21 12:03:34','2014-04-21 12:03:34'),(67,'Site.Chat.Url','https://www.google.com/',0,0,NULL,'text',NULL,'2014-04-21 12:03:34','2014-04-21 12:03:34'),(73,'Site.Google.GTM','',1,1,NULL,'textarea',NULL,'2015-10-14 09:58:23','2015-10-14 09:58:26'),(78,'Site.Email.ToDev','cleber.alves@fatordigital.com.br',1,1,NULL,'textarea',NULL,'2015-10-28 10:46:34','2015-10-28 10:46:35');

/*Table structure for table `svn` */

DROP TABLE IF EXISTS `svn`;

CREATE TABLE `svn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` enum('Pagina','Noticia','Curso') NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `conteudo` text NOT NULL,
  `conteudo_rascunho` text,
  `obs` text,
  `registro_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `svn` */

/*Table structure for table `templates` */

DROP TABLE IF EXISTS `templates`;

CREATE TABLE `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET latin1 NOT NULL,
  `slug` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `conteudo` text CHARACTER SET latin1,
  `variaveis` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `templates` */

insert  into `templates`(`id`,`nome`,`slug`,`conteudo`,`variaveis`,`status`,`usuario_id`,`created`,`modified`) values (1,'Fale Conosco','fale_conosco','<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<title>{SITE_NOME}</title>\r\n</head>\r\n<body style=\"font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;\">\r\n<table width=\"98%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<table width=\"680\" align=\"center\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n	\r\n	<p style=\"margin-top: 0px; margin-bottom: 20px; text-align: center;\" ><img src=\"{SITE_URL}/img/email/email_logo_site.jpg\" alt=\"\" /></p>\r\n		\r\n	  <table style=\"border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;\">\r\n	    <thead>\r\n	      <tr>\r\n		<td style=\"font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;\" colspan=\"2\">CONTATO REALIZADO PELO SITE</td>\r\n	      </tr>\r\n	    </thead>\r\n	    <tbody>\r\n			<tr>\r\n				<td style=\"font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;\">\r\n					\r\n					<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n						<tbody>\r\n						<tr>\r\n							<td>Quem é:</td>\r\n							<td>{QUEM_EH}</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Nome:</td>\r\n							<td>{NOME}</td>\r\n						</tr>\r\n	<tr>\r\n							<td>E-mail:</td>\r\n							<td>{EMAIL}</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Telefone:</td>\r\n							<td>{TELEFONE}</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Mensagem:</td>\r\n							<td>{MENSAGEM}</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Data:</td>\r\n							<td>{DATA}</td>\r\n						</tr>\r\n						</tbody>\r\n					</table>\r\n				\r\n				</td>\r\n			</tr>\r\n	    </tbody>\r\n	  </table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</body>\r\n</html>\r\n','{SITE_NOME}, {SITE_URL}, {SAC_TIPO_NOME}, {NOME}, {EMAIL}, {TELEFONE}, {MENSAGEM}, {DATA}',1,NULL,'2015-01-20 22:28:44','2015-01-20 22:28:46'),(2,'Fale Conosco Interna','fale_conosco_interna','<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<title>{SITE_NOME}</title>\r\n</head>\r\n<body style=\"font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;\">\r\n<table width=\"98%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<table width=\"680\" align=\"center\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n	\r\n	<p style=\"margin-top: 0px; margin-bottom: 20px; text-align: center;\" ><img src=\"{SITE_URL}/img/email/email_logo_site.jpg\" alt=\"\" /></p>\r\n		\r\n	  <table style=\"border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;\">\r\n	    <thead>\r\n	      <tr>\r\n		<td style=\"font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;\" colspan=\"2\">CONTATO REALIZADO PELO SITE</td>\r\n	      </tr>\r\n	    </thead>\r\n	    <tbody>\r\n			<tr>\r\n				<td style=\"font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;\">\r\n					\r\n					<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n						<tbody>\r\n						<tr>\r\n							<td>Local:</td>\r\n							<td>{LOCAL}</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Url:</td>\r\n							<td>{URL}</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Nome:</td>\r\n							<td>{NOME}</td>\r\n						</tr>\r\n						<tr>\r\n							<td>E-mail:</td>\r\n							<td>{EMAIL}</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Telefone:</td>\r\n							<td>{TELEFONE}</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Mensagem:</td>\r\n							<td>{MENSAGEM}</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Data:</td>\r\n							<td>{DATA}</td>\r\n						</tr>\r\n						</tbody>\r\n					</table>\r\n				\r\n				</td>\r\n			</tr>\r\n	    </tbody>\r\n	  </table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</body>\r\n</html>\r\n','{SITE_NOME}, {SITE_URL}, {CURSO_NOME}, {NOME}, {EMAIL}, {TELEFONE}, {MENSAGEM}, {DATA}',1,NULL,'2015-01-20 22:28:44','2015-01-20 22:28:44');

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo_id` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ultimo_acesso` datetime DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL COMMENT 'quem mexeu aqui por último?',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `u_grupo_id` (`grupo_id`),
  CONSTRAINT `u_grupo_id` FOREIGN KEY (`grupo_id`) REFERENCES `grupos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id`,`grupo_id`,`nome`,`email`,`senha`,`telefone`,`status`,`ultimo_acesso`,`usuario_id`,`created`,`modified`) values (1,1,'Cléber Alves','cleber.alves@fatordigital.com.br','26dcc95d0f7d45f924059d16a99e4c7ec86e8036',NULL,1,'2016-03-14 19:43:10',1,'2016-03-14 19:08:51','2016-03-14 19:43:10');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
