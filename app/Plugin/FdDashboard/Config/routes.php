<?php
    Router::connect("/fatorcms/dashboard", array(
            'plugin'     => 'fd_dashboard',
            'controller' => 'fd_dashboard',
            'action'     => 'index',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );

    Router::connect("/fatorcms/dashboard/client/:id", array(
        'plugin'     => 'fd_dashboard',
        'controller' => 'fd_dashboard',
        'action'     => 'client',
        'prefix'     => 'fatorcms',
        'fatorcms'   => true
    ), array(
            'pass' => array('id'),
            'id'   => '[0-9]+'
        )
    );

    Router::connect("/fatorcms/dashboard/:action/*", array(
            'plugin'     => 'fd_dashboard',
            'controller' => 'fd_dashboard',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );
