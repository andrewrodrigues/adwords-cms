<?php
    header("Access-Control-Allow-Origin: *");

    class FdDashboardController extends FdDashboardAppController
    {

        public $uses = array('FdClientes.Cliente', 'FdClientes.Timeline', 'FdClientes.Notificacao', 'FdUsuarios.Usuario');

        public function fatorcms_index()
        {

        }

        public function fatorcms_clients()
        {
            $this->layout = false;
            $this->render(false);
            $clients = $this->Cliente->find('all',
                array(
                    'fields' => array('Cliente.user_id', 'Cliente.notifications', 'Cliente.name', 'Cliente.status')
                )
            );
            if (!$clients) {
                die($this->jsonReturn(true, 'Nenhum cliente encontrado no momento.'));
            } else {
                die($this->jsonReturn(false, 'Clientes encontrados', $clients));
            }
        }

        public function fatorcms_client($id)
        {
            if (!$id)
                $this->redirect($this->referer());
            else {
                $client = $this->Cliente->findById($id);
                if (!$client) {
                    $this->Session->setFlash('Cliente não encontrado', 'fatorcms_danger');
                    $this->redirect($this->referer());
                } else {
                    $this->set(compact('client'));
                }
            }
        }

        public function fatorcms_savenotification()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST.', array(), 500));
            else {
                $data = $this->request->data;
                if (!isset($data['from_user_id']) || !isset($data['to_user_id']) || !isset($data['text']))
                    die($this->jsonReturn(true, 'Verifique se todos os campos foram preenchidos.'));
                else {
                    $saveNotificacao['Notificacao'] = array(
                        'from_user_id' => $data['from_user_id'],
                        'to_user_id'   => $data['to_user_id'],
                        'text'         => $data['text'],
                        'type'         => $data['type'],
                        'alert_date'   => $data['alert_date']
                    );
                    if (!$this->Notificacao->save($saveNotificacao))
                        die($this->jsonReturn(true, 'Houve um erro ao salvar a notificação'));
                    else {
                        die($this->jsonReturn(false, 'Notificação enviada'));
                    }
                }
            }
        }

        public function fatorcms_savepost()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST.', array(), 500));
            else {
                $data = $this->request->data;
                if (!isset($data['text']) && !isset($data['client_id']))
                    die($this->jsonReturn(true, 'Informe um texto e o ID do cliente'));
                else {
                    $saveData['Timeline'] = array(
                        'user_id'   => $data['user_id'],
                        'client_id' => $data['client_id'],
                        'text'      => $data['text'],
                        'status'    => true
                    );
                    if (!$this->Timeline->save($saveData))
                        die($this->jsonReturn(true, 'Não foi possível salvar na sua timeline, atualize a página e tente novamente.'));
                    else {
                        //$posts = $this->getClientTimeline($data);
                        die($this->jsonReturn(false, 'O Registro foi salvo com sucesso.'));
                    }
                }
            }
        }

        public function fatorcms_updatepost()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST.', array(), 500));
            else {
                $data = $this->request->data;
                if (!isset($data['text']) && !isset($data['client_id']) && !isset($data['id']))
                    die($this->jsonReturn(true, 'Informe um texto e o ID do cliente e o ID do POST'));
                else {
                    $updateData['Timeline'] = array(
                        'id'        => $data['id'],
                        'user_id'   => $data['user_id'],
                        'client_id' => $data['client_id'],
                        'text'      => $data['text'],
                        'status'    => true
                    );
                    if (!$this->Timeline->save($updateData))
                        die($this->jsonReturn(true, 'Não foi possível atualizar este POST, atualize a página e tente novamente.'));
                    else {
                        $posts = $this->getClientTimeline($data);
                        if ($posts)
                            die($this->jsonReturn(false, 'Registros encontrados para este cliente.', $posts));
                        die($this->jsonReturn(false, 'Nenhum registro informado para esse cliente.'));
                    }
                }
            }
        }

        public function fatorcms_getposts()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST.', array(), 500));
            else {
                $data = $this->request->data;
                if (!isset($data['client_id']))
                    die($this->jsonReturn(true, 'O "ID" do cliente não foi encontrado, por favor, atualize a página ou entre em contato com o desenvolvedor.', array(), 500));
                else {
                    $posts = $this->getClientTimeline($data);
                    if ($posts)
                        die($this->jsonReturn(false, 'Registros encontrados para este cliente.', $posts));
                    die($this->jsonReturn(false, 'Nenhum registro informado para esse cliente.'));
                }
            }
        }

        public function fatorcms_getnotifications()
        {
            $this->layout = false;
            $this->render(false);
            if ($this->request->is('post') && isset($this->request->data['user_id']))
                die($this->jsonReturn(false, 'Retorno de notificações', $this->Notificacao->getNotificationByUser($this->request->data['user_id'])));
            else {
                die;
            }
        }

        public function fatorcms_readnotification()
        {
            $this->layout = false;
            $this->render(false);
            if ($this->request->is('post') && isset($this->request->data['id']) && isset($this->request->data['user_id'])) {

                $data = $this->request->data;
                $this->Notificacao->id = $data['id'];
                $this->Notificacao->saveField('read', true);
                $this->Notificacao->save();

                die($this->jsonReturn(false, 'Retorno de notificações', $this->Notificacao->getNotificationByUser($data['user_id'])));
            } else {
                die;
            }
        }

        public function fatorcms_getusers()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST', array()));
            else {
                $data = $this->request->data;
                die($this->jsonReturn(true, 'Usuários encontrados no sistema.', $this->Usuario->find('list', array('fields' => array('Usuario.id', 'Usuario.nome'), 'conditions' => array('Usuario.id <>' => $data['user_id']))), 200));
            }
        }

        public function fatorcms_delete()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST.', array(), 500));
            else {
                $data = $this->request->data;
                if (!isset($data['client_id']) || !isset($data['id']))
                    die($this->jsonReturn(true, 'O "ID" do cliente ou "ID" do post não foi encontrado, por favor, atualize a página ou entre em contato com o desenvolvedor.', array(), 500));
                else {

                    $deleteData = $this->Timeline->find('first',
                        array(
                            'recursive'  => -1,
                            'conditions' => array(
                                'Timeline.id' => $data['id']
                            )
                        )
                    );
                    $deleteData['Timeline']['deleted'] = date('Y-m-d H:i:s');

                    if (!$this->Timeline->save($deleteData))
                        die($this->jsonReturn(true, 'Não foi possível remove este POST, atualize a página e tente novamente.'));
                    else {
                        $posts = $this->getClientTimeline($data);
                        if ($posts)
                            die($this->jsonReturn(false, 'Registros encontrados para este cliente.', $posts));
                        die($this->jsonReturn(false, 'Nenhum registro informado para esse cliente.'));
                    }
                }
            }
        }

        private function getClientTimeline($data)
        {
            $return = $this->Cliente->find('first',
                array(
                    'conditions' => array(
                        'Cliente.id' => $data['client_id']
                    )
                )
            );
            $return['Timeline'] = $this->Timeline->reorderTimeline($return['Timeline']);
            return $return;
        }

        private function jsonReturn($error, $msg, $data = array(), $status = 200)
        {
            return json_encode(
                array(
                    'error'  => $error,
                    'data'   => $data,
                    'msg'    => $msg,
                    'status' => $status
                )
            );
        }

    }
