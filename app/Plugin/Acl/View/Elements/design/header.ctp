<?php echo $this->Html->css('/acl/css/acl.css'); ?>

<?php $this->Html->addCrumb('Controle de Acessos') ?>

<div id="plugin_acl">

	<?php echo $this->Session->flash('plugin_acl'); ?>
	<div class="row">
    	<h3 class="col-lg-12 pull-left"><?php echo __d('acl', 'Controle de Acesso'); ?></h3>
	</div>

	<?php
		if(!isset($no_acl_links)){
		    $selected = isset($selected) ? $selected : $this->params['controller'];

	        $links = array();
	        //$links[] = $this->Html->link(__d('acl', 'Permissões'), '/fatorcms/acl/aros/index', array('class' => ($selected == 'aros' )? 'selected' : null));
	        //$links[] = $this->Html->link(__d('acl', 'Ações'), '/fatorcms/acl/acos/index', array('class' => ($selected == 'acos' )? 'selected' : null));

	        echo $this->Html->nestedList($links, array('class' => 'acl_links'));
		}
	?>