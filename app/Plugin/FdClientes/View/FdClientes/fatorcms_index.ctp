<?php $this->Html->addCrumb('Clientes') ?>

<div class="row">
    <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        Clientes
        <?php echo $this->Html->link('Cadastrar Clientes', array('action' => 'add'), array('class' => 'btn btn-info pull-right')); ?>
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                        <div class="form-group">
                            <?php echo $this->FilterForm->input('filtro_name', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                        </div>
                        <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                        <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->Html->link('<i class="fa fa-share-square-o"></i>&nbsp;Exportar', $this->params->params['named'] + array('acao' => 'exportar'), array('class' => 'btn btn-info btn-small btn-export', 'escape' => false)); ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('/fatorcms/limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th class="text-center"><?php echo $this->Paginator->sort('nome', 'Nome') ?></th>
                            <th class="text-center"><?php echo $this->Paginator->sort('modified', 'Modificação') ?></th>
                            <th class="text-center">Notificações</th>
                            <th class="text-center"
                                style="width:80px"><?php echo $this->Paginator->sort('status', 'Ativo') ?></th>
                            <th class="text-center"
                                style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($registers) > 0): ?>
                            <?php foreach ($registers AS $register): ?>
                                <tr>
                                <td><?php echo $register['Cliente']['id'] ?></td>
                                <td class="text-center"><?php echo $register['Cliente']['name'] ?></td>
                                <td class="text-center"><?php echo $this->Time->format($register['Cliente']['modified'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td class="text-center"><?php echo $register['Cliente']['notifications']; ?></td>
                                <td>
                                    <input type="checkbox"
                                           class="atualiza-status"
                                           value="<?php echo $register['Cliente']['status'] == 1 ? 0 : 1 ?>"
                                           data-id="<?php echo $register['Cliente']['id'] ?>"
                                           data-url="<?php echo $this->Html->url(array('action' => 'status')) ?>"
                                           data-on-text="Sim"
                                           data-on-color="success"
                                           data-off-text="Não"
                                           data-off-color="danger"<?php echo $register['Cliente']['status'] == 1 ? ' checked="checked"' : '' ?>>
                                </td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $register['Cliente']['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $register['Cliente']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $register['Cliente']['name'])) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="6"
                                    class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('/fatorcms/paginator'); ?>
            </div>
        </section>
    </div>
</div>