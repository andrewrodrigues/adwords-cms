<?php
    Router::connect("/fatorcms/clientes", array(
            'plugin'     => 'fd_clientes',
            'controller' => 'fd_clientes',
            'action'     => 'index',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );
    Router::connect("/fatorcms/clientes/:action/*", array(
            'plugin'     => 'fd_clientes',
            'controller' => 'fd_clientes',
            'prefix'     => 'fatorcms',
            'fatorcms'   => true
        )
    );