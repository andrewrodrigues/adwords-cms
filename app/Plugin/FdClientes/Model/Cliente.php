<?php
    App::uses('AuthComponent', 'Controller/Component');

    class Cliente extends FdClientesAppModel
    {

        public $useTable = 'clients';

        public $validate = array(
            'name'       => array(
                'required' => array(
                    'rule'     => array('notEmpty'),
                    'message'  => 'Informe o nome do cliente',
                    'required' => true,
                ),
                'unique'   => array(
                    'rule'    => array('isUnique'),
                    'message' => 'O cliente já está cadastrado.',
                    'on'      => 'create',
                )
            )
        );

        public $hasMany = array(
          'Timeline' => array(
            'className' => 'FdClientes.Timeline',
            'foreignKey' => 'client_id',
            'conditions' => array(
              'Timeline.deleted' => ''
            ),
            'order' => 'Timeline.created DESC'
          )
        );

        public function parentNode()
        {
        }

        public function addClient($data)
        {
            return parent::save($data) ? true : false;
        }

        public function updateClient($data)
        {
            return parent::save($data) ? true : false;
        }

        /**
         * @param $id
         * @return bool
         * Verifica se não existe dependencia e deleta
         */
        public function deleteClient($id)
        {
            $this->id = $id;
            if ($this->id)
                return parent::delete($id) ? true : false;
            else {
                return false;
            }
        }

        public function getClients()
        {
            return parent::find('all');
        }

    }
