<?php
    App::uses('AuthComponent', 'Controller/Component');

    class Timeline extends FdClientesAppModel
    {

        public $useTable = 'timelines';

        public function parentNode()
        {
        }

        public $belongsTo = array(
            'Cliente' => array(
                'className'  => 'FdClientes.Cliente',
                'foreignKey' => 'client_id',
                'table'      => 'clients'
            )
        );

        public function reorderTimeline($id)
        {
            $query = $this->find('all',
                array(
                    'recursive'  => '-1',
                    'conditions' => array(
                        'Timeline.client_id' => $id,
                        'Timeline.deleted'   => null
                    )
                )
            );
            if (!$query) {
                return false;
            } else {
                $refactor = array();
                foreach ($query as $row) {
                    $sql = new DateTime($row['Timeline']['created']);
                    $date = new DateTime();
                    $date->setTime(0, 0, 0);
                    $date->setDate($sql->format('Y'), $sql->format('m'), $sql->format('d'));
                    $refactor[strval($date->format('U') * 1000)][] = $row['Timeline'];
                }
                return $refactor;
            }
        }

    }

    //    date('Y-m-d', strtotime($row['Timeline']['created']))
    //    $date = new DateTime(date('Y-m-d'));
    //    $date->format('U') * 1000;
    //
    //    public function reorderTimeline ($registers) {
    //    if (!is_array($registers) || count($registers) < 1)
    //        return false;
    //    else {
    //        $refactor = array();
    //        foreach($registers as $register) {
    //            $date = new DateTime($register['created']);
    //            $refactor[$date->format('d/m')][] = $register;
    //        }
    //        return (!empty($refactor)) ? $refactor : $registers;
    //    }
    //}