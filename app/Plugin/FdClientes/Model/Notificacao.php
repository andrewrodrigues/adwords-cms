<?php
    App::uses('AuthComponent', 'Controller/Component');

    class Notificacao extends FdClientesAppModel
    {
        public $useTable = 'notifications';

        public function getNotificationByUser ($user_id) {
          return parent::find('all',
            array(
              'conditions' => array(
                'Notificacao.to_user_id' => $user_id,
                'Notificacao.read' => 0
              )
            )
          );
        }

    }
