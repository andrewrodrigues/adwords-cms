<?php

    class FdClientesController extends FdClientesAppController
    {
        public $uses = array('FdClientes.Cliente');

        public function beforeFilter()
        {
            parent::beforeFilter();
        }

        public function fatorcms_index($page = 1)
        {

            $this->FilterResults->addFilters(
                array(
                    'filtro_name' => array(
                        'name' => array(
                            'operator' => 'LIKE',
                            'value'    => array('before' => '%', 'after' => '%'),
                        ),
                    ),
                )
            );

            $this->FilterResults->setPaginate('page', $page);

            $options['conditions'] = $this->FilterResults->getConditions();

            $options['order'] = 'id ASC';
            $this->paginate = $options;

            // Exportar?
            if (isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar") {
                $this->Reports->xls($this->Cliente->find('all', array('conditions' => $options['conditions'], 'recursive' => -1, 'callbacks' => false)), 'Clientes');
            }

            // Paginate
            $registers = $this->paginate();
            $this->set(compact('registers'));
        }

        /**
         * fatorcms_status method
         *
         * @return void
         */
        public function fatorcms_status()
        {
            if (!$this->request->is('post')) {
                throw new NotFoundException('Registro inválido.');
            }
            die($this->_saveStatus('Cliente', $this->request->data['id'], $this->request->data['value']));
        }

        public function fatorcms_add()
        {
            if ($this->request->is('post'))
                $this->Cliente->addClient($this->request->data) ? $this->Session->setFlash('Registro salvo com sucesso.', 'fatorcms_success') : $this->Session->setFlash('Ops, não foi possível salvar o registro. Tente Novamente.', 'fatorcms_danger');
        }

        public function fatorcms_edit($id)
        {
            if ($this->request->is('put'))
                $this->Cliente->updateClient($this->request->data) ? $this->Session->setFlash('Registro alterado com sucesso.', 'fatorcms_success') : $this->Session->setFlash('Ops, não foi possível alterar o registro. Tente novamente.', 'fatorcms_danger');
            else {
                $this->data = $this->Cliente->findById($id);
            }
        }

        public function fatorcms_delete($id)
        {
            if (!empty($id))
                $this->Cliente->deleteClient($id) ? $this->Session->setFlash('Registro foi deletado com sucesso.', 'fatorcms_success') : $this->Session->setFlash('Ops, não foi possível remover o registro. Tente novamente.', 'fatorcms_danger');
            else if ($this->request->is('get')) {
                throw new MethodNotAllowedException();
            }
            $this->_redirectFilter($this->referer());
        }

    }