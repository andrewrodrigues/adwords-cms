<?php $this->Html->addCrumb('E-mail', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Detalhe do E-mail') ?>

<h3>Detalhe do E-mail</h3>

<div class="panel">
	<div class="panel-body">
		<div class="row">
			<div class="col-lg-6">
				<?php if (is_object(json_decode($email['Email']['to']))):  ?>
					<strong>To: </strong> <?php echo implode('/', json_decode($email['Email']['to'], true)); ?>
				<?php else: ?>
					<strong>To: </strong> <?php echo $email['Email']['to']; ?>
				<?php endIf; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>Subject: </strong> <?php echo $email['Email']['subject']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>Status: </strong> <?php echo ($email['Email']['status'] == true) ? 'Enviado' : 'Não enviado'; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>Enviado: </strong> <?php echo $this->Time->format($email['Email']['created'], '%d/%m/%Y %H:%M:%S') ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>Conteúdo do E-mail: </strong> <?php echo $email['Email']['content']; ?>
			</div>
		</div>
	</div>
</div>