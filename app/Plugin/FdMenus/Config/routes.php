<?php
	Router::connect("/fatorcms/menus", array('plugin' => 'fd_menus', 'controller' => 'fd_menus', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/menus/:action/*", array('plugin' => 'fd_menus', 'controller' => 'fd_menus', 'prefix' => 'fatorcms', 'fatorcms' => true));

	Router::connect("/fatorcms/links", array('plugin' => 'fd_menus', 'controller' => 'fd_links', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/links/:action/*", array('plugin' => 'fd_menus', 'controller' => 'fd_links', 'prefix' => 'fatorcms', 'fatorcms' => true));