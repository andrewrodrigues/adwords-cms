<?php $this->Html->addCrumb('Menus', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Menu') ?>

<h3>Cadastrar Menu</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Menu', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label>Status</label>
					<div class="icheck">
						<?php echo $this->Form->input('status', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																	'default' => 1, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('nome', array('class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('slug', array('class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('descricao', array('class' => 'form-control', 'label' => 'Descrição')) ?>
				</div>
			</div>
		</div>
		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
		<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
	<?php echo $this->Form->end() ?>
	</div>
</div>