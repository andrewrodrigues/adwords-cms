<?php

class MenusHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array(
        'Html'
    );

/**
 * Constructor
 *
 * @param array $options options
 * @access public
 */
	
	public function __construct($options = array()) {
		return parent::__construct($options);
    }

/**
 * Show Menu by Alias
 *
 * @param string $menuAlias Menu alias
 * @param array $options (optional)
 * @return string
 */
    public function getMenu($menuAlias, $options = array()) {
        $_options = array(
            'findOptions' => array(),
            'tag' => 'ul',
            'tagAttributes' => array('class' => 'nav nav-list'),
            'containerTag' => 'div',
            'containerTagAttr' => array(
                'class' => 'menu',
            ),
            'selected' => 'active',
            'dropdown' => false,
            'dropdownClass' => 'sf-menu',
        );
        $options = array_merge($_options, $options);
		
		if (!isset($this->_View->viewVars['menus_for_layout'][$menuAlias])) {
            return false;
        }
        $menu = $this->_View->viewVars['menus_for_layout'][$menuAlias];

        $options['containerTagAttr']['id'] = 'menu-' . $this->_View->viewVars['menus_for_layout'][$menuAlias]['Menu']['id'];
        $options['containerTagAttr']['class'] .= '';

        $links = $this->_View->viewVars['menus_for_layout'][$menuAlias]['threaded'];
        $linksList = $this->_nestedLinks($links, $options);
        $output = $this->Html->tag($options['containerTag'], $linksList, $options['containerTagAttr']);

        return $output;
    }
/**
 * Nested Links
 *
 * @param array $links model output (threaded)
 * @param array $options (optional)
 * @param integer $depth depth level
 * @return string
 */
    private function _nestedLinks($links, $options = array(), $depth = 1) {
        $_options = array();
        $options = array_merge($_options, $options);
		
		if($depth > 1){
			$options['tagAttributes'] = array('class' => 'sub');
		}
		
		if(isset($options['registro_coluna']) && $options['registro_coluna'] != ""){
			$options['registro_linha'] = ceil(count($links)/$options['registro_coluna']);
		}
		
        $output = '';
		$cont = 1;
        foreach ($links AS $link) {
            $linkAttr = array(
		        'id' 		=> 'link-' . $link['Link']['id'],
                'rel' 		=> $link['Link']['rel'],
                'target' 	=> $link['Link']['target'],
                'title' 	=> $link['Link']['description'],	
				'escape' 	=> false,
            );

            foreach ($linkAttr AS $attrKey => $attrValue) {
                if ($attrValue == null && $attrValue != false) {
                    unset($linkAttr[$attrKey]);
                }
            }

            if($link['Link']['class'] != ""){
                $title = '<i class="'.$link['Link']['class'].'"></i>';
            }else{
                $title = '<i class="icon-double-angle-right"></i>';
            }
			$title .= '<span class="menu-text">'.$link['Link']['title'].'</span>';
			if (isset($link['children']) && count($link['children']) > 0) {
				$title .= '<b class="arrow icon-angle-down"></b>';
				$linkAttr['class'] = "dropdown-toggle";
			}

            $url_atual = Router::url('/' . $this->params->url, true);

            $linkOptions = array();

            $linkOutput = $this->Html->link($title, $link['Link']['seo_url'], $linkAttr);
            if (isset($link['children']) && count($link['children']) > 0) {

                $linkOutput .= $this->_nestedLinks($link['children'], $options, $depth + 1);
				
				foreach($link['children'] as $children){
					// if($url_atual == Router::url($children['Link']['link'], true)){
                    if(stripos($url_atual, $children['Link']['seo_url']) != false || strpos($children['Link']['seo_url'], $this->params->params['controller']) != false){
						$linkOptions = array('class' => $options['selected']);
					}elseif(stripos($url_atual, '/links/') != false && $link['Link']['title'] == 'Configurações'){
                        $linkOptions = array('class' => $options['selected']);
                    }elseif(stripos($url_atual, '/usuario_enderecos/') != false && $link['Link']['title'] == 'Usuários'){
                        $linkOptions = array('class' => $options['selected']);
                    }

                    if (isset($children['children']) && count($children['children']) > 0) {
                        foreach($children['children'] as $children2){
                            if(stripos($url_atual, $children2['Link']['seo_url']) != false || strpos($children2['Link']['seo_url'], $this->params->params['controller']) != false){
                                $linkOptions = array('class' => $options['selected']);
                            }
                        }
                    }

				}
            }
			
			if($link['Link']['seo_url'] == '/admin'){
                if (Router::url($link['Link']['seo_url'], true) == $url_atual) {
                    $linkOptions = array('class' => $options['selected']);
                }
            }elseif(stripos($url_atual, $link['Link']['seo_url']) != false || strpos($link['Link']['seo_url'], $this->params->params['controller']) != false){
              if($link['Link']['seo_url'] == '/admin/produtos' && $this->params->params['action'] != "admin_edit_multiple"){
                $linkOptions = array('class' => $options['selected']);
              }elseif($link['Link']['seo_url'] == '/admin/produtos/edit_multiple' && $this->params->params['action'] == "admin_edit_multiple"){
                $linkOptions = array('class' => $options['selected']);
              }elseif($link['Link']['seo_url'] != '/admin/produtos' && $link['Link']['seo_url'] != '/admin/produtos/edit_multiple'){
                $linkOptions = array('class' => $options['selected']);
              }
            }elseif(stripos($url_atual, '/links/') != false && $link['Link']['seo_url'] == '/admin/menus'){
                $linkOptions = array('class' => $options['selected']);
            }elseif(stripos($url_atual, '/usuario_enderecos/') != false && $link['Link']['title'] == 'Usuários'){
                $linkOptions = array('class' => $options['selected']);
            }
			
			$linkOutput = $this->Html->tag('li', $linkOutput, $linkOptions);
			
			$output .= $linkOutput;
			
			if(isset($options['registro_linha']) && $options['registro_linha'] != ""){
				if($cont%$options['registro_linha'] == 0){
					 $output .= "</ul><ul>";
				}
			}
			
			$cont++;
        }
        if ($output != null) {
            $tagAttr = $options['tagAttributes'];
            if ($options['dropdown'] && $depth == 1) {
                $tagAttr['class'] = $options['dropdownClass'];
            }
            $output = $this->Html->tag($options['tag'], $output, $tagAttr);
        }

        return $output;
    }
}
?>