<?php

class FdMenusAppController extends AppController {

	public function beforeFilter(){
        parent::beforeFilter();

        App::import('Model', 'FdUsuarios.Grupo');
        $this->Grupo = new Grupo();
    }

}