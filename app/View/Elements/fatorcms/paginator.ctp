<p><?php echo $this->Paginator->counter('Página <strong>{:page}</strong> de <strong>{:pages}</strong>, mostrando <strong>{:current}</strong> registros no total de <strong>{:count}</strong> registros.') ?></p>
<?php if ($this->Paginator->hasPage(2)): ?>
	<?php
		if(isset($options)){
			$this->Paginator->options($options);
		}
	?>
	<ul class="pagination">
		<?php echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ); ?>
		<?php echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) ) ?>
		<?php echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ) ?>
	</ul>
<?php endif; ?>