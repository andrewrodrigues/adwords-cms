<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php echo $this->Html->charset() ?>
    <title>
        <?php
            //seo_title
            if($this->fetch('title')){
                $seo['title'] = '';
                if(isset($this->params->params['page']) && $this->params->params['page'] > 1){
                    $seo['title'] = $this->fetch('title') . ' | Página ' . $this->params->params['page'];
                }else{
                    $seo['title'] = $this->fetch('title');
                }

                $seo['title'] .= ' | '. Configure::read('Site.Seo.Title');
            }elseif($title_for_layout != ""){
                $seo['title'] = $title_for_layout;

                if(isset($this->params->params['page']) && $this->params->params['page'] > 1){
                    $seo['title'] .= ' | Página ' . $this->params->params['page'];
                }

                $seo['title'] .= ' | '. Configure::read('Site.Seo.Title');
            }else{
                $seo['title'] = Configure::read('Site.Seo.Title');
            }
        ?>
        <?php echo $seo['title']; ?>
    </title>

    <?php echo $this->Html->meta('icon') ?>
    <?php
        //seo_keywords
        if($this->fetch('keywords')){
            $seo['keywords'] = $this->fetch('keywords');
        }else{
            $seo['keywords'] = Configure::read('Site.Seo.Keywords');
        }

        //seo_description
        if($this->fetch('description')){
            $seo['description'] = $this->fetch('description');
        }else{
            $seo['description'] = Configure::read('Site.Seo.Description');
        }

        //meta
        echo $this->Html->meta('keywords', $seo['keywords']);
        echo $this->Html->meta('description', $seo['description']);
    ?>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?php //echo $this->element('site/metatags', array('seo' => $seo)); ?>

    <?php if(Configure::read("Site.Google.GTM") != ""): ?>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=<?php echo Configure::read("Site.Google.GTM"); ?>"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','<?php echo Configure::read("Site.Google.GTM"); ?>');</script>
        <!-- End Google Tag Manager -->
    <?php elseif(Configure::read("Site.Google.Analytics") != ""): ?>
        <script type="text/javascript">
             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
             m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
             })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

             ga('create', '<?php echo Configure::read("Site.Google.Analytics"); ?>', 'auto');
             ga('require', 'displayfeatures');
             ga('send', 'pageview');
        </script>
    <?php endIf; ?>

    <?php
        echo $this->fetch('meta');
        echo $this->fetch('css');
        //echo $this->fetch('css_interna');
        echo $this->fetch('script');
    ?>
</head>

<body class="<?php echo $this->fetch('body') ?>" rel="<?php echo $this->params['controller'] ?>" data-url="<?php echo isset($this->params->params['pass'][1]) ? $this->params->params['pass'][1] : $this->params->url; ?>">

    <?php echo $this->Session->flash() ?>
    <?php echo $this->fetch('content') ?>


</body>

</html>