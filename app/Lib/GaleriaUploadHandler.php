<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

require('UploadHandler.php');
require('Wideimage/WideImage.php');

class GaleriaUploadHandler extends UploadHandler {

	protected function initialize() {
        //ClassRegistry::init("Campus");
        App::import('Model', 'FdGalerias.GaleriaImagem');
        $this->GaleriaImagem = new GaleriaImagem();

        $this->options['script_url'] = $this->options['url_root'].'fatorcms/galerias/upload/';
        $this->options['upload_dir'] = dirname($this->get_server_var('SCRIPT_FILENAME')).'/files/galeria/file/'.$this->options['path_id'].'/';
        $this->options['upload_url'] = $this->options['url_root'].'files/galeria/file/'.$this->options['path_id'].'/';

        parent::initialize();
    }


    //sobrecarga do metodo que obtem informacoes extra, além das infos das imagens
    protected function handle_form_data($file, $index) {
        // $file->ordem = @$_REQUEST['ordem'][$index];
        $file->legenda = @$_REQUEST['legenda'][$index];
        $file->galeria_id = @$_REQUEST['galeria_id'][$index];
    }

    //sobrecarga do metodo que faz o upload, para inserir as fotos no banco, com o vinculo necessario
    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null) {
        $tmp_name = $name;
        $tmp_name = explode('.', $tmp_name);
        $name = strtolower(Inflector::slug($tmp_name[0], '-')).'-'.time();

        $fileDir = $name . '.' . $tmp_name[1];

        $exist = $this->options['upload_dir'] . $fileDir;

        if(file_exists($exist)){
            $count = count(glob($dir . '*.*'));
            $name = $name . '-' . $count;
        }

        $file = parent::handle_file_upload(
            $uploaded_file, $name, $size, $type, $error, $index, $content_range
        );
        if (empty($file->error)) {

            $file_name = $file->name;

            $data = array(
                    'id' => null,
                    'galeria_id' => $file->galeria_id,
                    'file' => $file_name,
                    'dir' => $file->galeria_id,
                    'type' => $file->type,
                    'size' => $file->size,
                    'legenda' => $file->legenda,
                    'created' => date('Y-m-d H-i-s'),
                    'modified' => date('Y-m-d H-i-s'),
                    );
            $this->GaleriaImagem->save($data);

            $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'galeria'. DS  . 'file' . DS . $file->galeria_id;
            $imagem = $dir . DS . $file->name;

            if(is_file($imagem)){
                //800, 600
                $novo_dir   = $dir.DS.'zoom';
                $outFile    = $novo_dir.DS.$file->name;
                if(!is_dir($novo_dir)){
                    if(mkdir($novo_dir)){
                        WideImage::load($imagem)->resize(800)->crop('center', 'center', 800, 800)->saveToFile($outFile);
                    }
                }else{
                    WideImage::load($imagem)->resize(800)->crop('center', 'center', 800, 800)->saveToFile($outFile);
                }
            }

            $file->id = $this->GaleriaImagem->id;
        }
        return $file;
    }

    //sobrecarga do metodo que retorna as infos de cada imagem
    protected function set_additional_file_properties($file) {
        parent::set_additional_file_properties($file);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $GaleriaImagem = $this->GaleriaImagem->find('first', array('recursive' => -1, 'conditions' => array("GaleriaImagem.file LIKE '%$file->name%'")));
            if($GaleriaImagem){
                $file->id = $GaleriaImagem['GaleriaImagem']['id'];
                $file->type = $GaleriaImagem['GaleriaImagem']['type'];
                $file->legenda = $GaleriaImagem['GaleriaImagem']['legenda'];
                $file->galeria_id = $GaleriaImagem['GaleriaImagem']['galeria_id'];
            }
        }
    }

     //sobrecarga do metodo que retorna as infos de cada imagem
    protected function get_file_objects($iteration_method = 'get_file_object') {
        $images = parent::get_file_objects($iteration_method);
        $return_images = array();
        if(count($images) > 0){
            foreach ($images as $key => $img) {
                if(!isset($img->id)){
                    unset($images[$key]);
                }else{
                    $return_images[] = $img;
                }
            }
        }

        return $return_images;
    }

    //sobrecarga do metodo que deleta a imagem, para que a mesma seja deletada do banco também
    public function delete($print_response = true) {
        $response = parent::delete(false);
        foreach ($response as $name => $deleted) {
            if ($deleted) {
                $extract = array_reverse(explode(".", $name));

                $GaleriaImagem = $this->GaleriaImagem->find('first', array('recursive' => -1, 'conditions' => array("GaleriaImagem.file LIKE '%$name%'")));
                if($GaleriaImagem){
                    $this->GaleriaImagem->delete($GaleriaImagem['GaleriaImagem']['id']);

                    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'campus'. DS  . 'file' . DS . $GaleriaImagem['GaleriaImagem']['galeria_id'];
                    if(file_exists($dir . DS . $name)){
                        unlink($dir . DS . $name);
                    }

                    if(file_exists($dir . DS . 'zoom' . DS . $name)){
                        unlink($dir . DS . 'zoom' . DS . $name);
                    }

                    if(file_exists($dir . DS . 'thumb' . DS . $name)){
                        unlink($dir . DS . 'thumb' . DS . $name);
                    }

                    if(file_exists($dir . DS . 'medium' . DS . $name)){
                        unlink($dir . DS . 'medium' . DS . $name);
                    }

                    if(file_exists($dir . DS . 'semi-thumbnail' . DS . $name)){
                        unlink($dir . DS . 'semi-thumbnail' . DS . $name);
                    }

                    if(file_exists($dir . DS . 'thumbnail' . DS . $name)){
                        unlink($dir . DS . 'thumbnail' . DS . $name);
                    }
                }
            }
        } 
        return $this->generate_response($response, $print_response);
    }

}
