<?php
    header("Access-Control-Allow-Origin: *");

    class ApiController extends AppController
    {

        public $uses = array('FdClientes.Cliente', 'FdClientes.Timeline', 'FdClientes.Notificacao', 'FdUsuarios.Usuario');

        public function beforeRender()
        {
            parent::beforeRender();
            $this->layout = false;
            $this->render(false);
        }

        public function beforeFilter()
        {
            parent::beforeFilter();
            $this->Auth->allow('getClient', 'getClientData', 'storeClientData', 'getLastTimelineId', 'authLogin', 'editClientData', 'destroyClientData', 'storeClient');
        }

        public function getLastTimelineId()
        {
            $query = $this->Timeline->find('first',
                array(
                    'recursive' => '-1',
                    'fields'    => array('Timeline.id'),
                    'order'     => array('Timeline.id DESC')
                )
            );
            die($this->jsonReturn(false, 'O último registro da timeline.', $query['Timeline']['id'] + 1));
        }

        public function getClient()
        {
            $clients = $this->Cliente->find('all',
                array(
                    'recursive' => '-1',
                    'fields'    => array('Cliente.id', 'Cliente.user_id', 'Cliente.notifications', 'Cliente.name', 'Cliente.status'),
                    'order'     => array('Cliente.name')
                )
            );
            if (!$clients) {
                die($this->jsonReturn(true, 'Nenhum cliente encontrado no momento.'));
            } else {
                die($this->jsonReturn(false, 'Clientes encontrados', $clients));
            }
        }

        public function getClientData($id)
        {
            if (!empty($id)) {
                $timeline = $this->Timeline->reorderTimeline($id);
                if ($timeline) {
                    die($this->jsonReturn(false, 'Registros encontrados.', $timeline));
                } else {
                    die($this->jsonReturn(true, 'Nenhum registro econtrado para este usuário'));
                }
            } else {
                die($this->jsonReturn(true, 'O ID do cliente não foi encontrado.', array(), 500));
            }
        }

        public function storeClientData()
        {
            $data = $this->request->data;
            if (!isset($data['client_id']) && !isset($data['text']))
                die($this->jsonReturn(true, 'Não foi preenchido o campo de informação ou não foi detectado o cliente que tu está cadastrando o a informação.', array(), 500));
            else {
                unset($data['id']);
                unset($data['date']);
                if ($this->Timeline->save($data)) {
                    die($this->jsonReturn(false, 'Informação adicionada com suceso'));
                } else {
                    die($this->jsonReturn(true, 'Aconteceu um erro inesperado, não foi possível salvar a informação.', array(), 500));
                }
            }
        }

        public function editClientData()
        {
            $data = $this->request->data;
            if (!isset($data['client_id']) && !isset($data['text']))
                die($this->jsonReturn(true, 'Não foi preenchido o campo de informação ou não foi detectado o cliente que tu está cadastrando o a informação.', array(), 500));
            else {
                unset($data['date']);
                unset($data['client_id']);
                if ($this->Timeline->save(array('Timeline' => $data))) {
                    die($this->jsonReturn(false, 'Informação adicionada com suceso'));
                } else {
                    die($this->jsonReturn(true, 'Aconteceu um erro inesperado, não foi possível salvar a informação.', array(), 500));
                }
            }
        }

        public function destroyClientData()
        {
            $data = $this->request->data;
            if (!isset($data['client_id']) && !isset($data['id']))
                die($this->jsonReturn(true, 'Desculpa, não foi possível remover este item. Tente novamente ou atualize a página.'));
            else {
                $destroy['Timeline'] = array(
                    'id'      => $data['id'],
                    'deleted' => date("Y-m-d H:i:s")
                );
                if ($this->Timeline->save($destroy)) {
                    die($this->jsonReturn(false, 'Informação foi removida com sucesso.'));
                } else {
                    die($this->jsonReturn(true, 'Aconteceu um erro inesperado, tente novamente ou atualize a página.', array(), 500));
                }
            }
        }

        public function authLogin()
        {
            if ($this->request->is('post')) {
                $data = $this->request->data;
                if (isset($data['user']) && isset($data['password'])) {
                    $auth = $this->Usuario->find('first',
                        array(
                            'conditions' => array(
                                'Usuario.email' => $data['user'],
                                'Usuario.senha' => AuthComponent::password($data['password'])
                            )
                        )
                    );
                    if ($auth) {
                        die($this->jsonReturn(false, 'Login efetuado com sucesso.', $auth, 200));
                    } else {
                        die($this->jsonReturn(true, 'Verifique se os dados foram digitados corretamente.', array(), 500));
                    }
                } else {
                    die($this->jsonReturn(true, 'Verifique se digitou os dados corretamente.', array(), 500));
                }
            }
            die($this->jsonReturn(true, 'Está página não existe.', array(), 500));
        }

        public function storeClient()
        {
            $data = $this->request->data;
            if (!isset($data['nome']))
                die($this->jsonReturn(true, 'Verifique se você preencheu todos os campos.', array(), 500));
            else {

                $query = $this->Cliente->find('first', array('conditions' => array('Cliente.name' => $data['nome'])));
                if (!$query) {
                    $save['Cliente'] = array(
                        'user_id' => $data['user_id'],
                        'name'    => $data['nome']
                    );
                    if ($this->Cliente->save($save)) {
                        $data = $this->Cliente->find('first', array('recursive' => '-1', 'conditions' => array('Cliente.id' => $this->Cliente->getLastInsertId())));
                        die($this->jsonReturn(false, 'Cadastro do cliente foi efetuado com sucesso..', $data, 200));
                    } else {
                        die($this->jsonReturn(true, 'Falha ao tentar salvar o cliente, tente novamente ou atualize a página.', array(), 500));
                    }
                } else {
                    die($this->jsonReturn(true, 'O Cliente já existe.', array(), 500));
                }
            }
        }


        public function client($id)
        {
            if (!$id)
                $this->redirect($this->referer());
            else {
                $client = $this->Cliente->findById($id);
                if (!$client) {
                    $this->Session->setFlash('Cliente não encontrado', 'danger');
                    $this->redirect($this->referer());
                } else {
                    $this->set(compact('client'));
                }
            }
        }

        public function savenotification()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST.', array(), 500));
            else {
                $data = $this->request->data;
                if (!isset($data['from_user_id']) || !isset($data['to_user_id']) || !isset($data['text']))
                    die($this->jsonReturn(true, 'Verifique se todos os campos foram preenchidos.'));
                else {
                    $saveNotificacao['Notificacao'] = array(
                        'from_user_id' => $data['from_user_id'],
                        'to_user_id'   => $data['to_user_id'],
                        'text'         => $data['text'],
                        'type'         => $data['type'],
                        'alert_date'   => $data['alert_date']
                    );
                    if (!$this->Notificacao->save($saveNotificacao))
                        die($this->jsonReturn(true, 'Houve um erro ao salvar a notificação'));
                    else {
                        die($this->jsonReturn(false, 'Notificação enviada'));
                    }
                }
            }
        }

        public function savepost()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST.', array(), 500));
            else {
                $data = $this->request->data;
                if (!isset($data['text']) && !isset($data['client_id']))
                    die($this->jsonReturn(true, 'Informe um texto e o ID do cliente'));
                else {
                    $saveData['Timeline'] = array(
                        'user_id'   => $data['user_id'],
                        'client_id' => $data['client_id'],
                        'text'      => $data['text'],
                        'status'    => true
                    );
                    if (!$this->Timeline->save($saveData))
                        die($this->jsonReturn(true, 'Não foi possível salvar na sua timeline, atualize a página e tente novamente.'));
                    else {
                        //$posts = $this->getClientTimeline($data);
                        die($this->jsonReturn(false, 'O Registro foi salvo com sucesso.'));
                    }
                }
            }
        }

        public function updatepost()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST.', array(), 500));
            else {
                $data = $this->request->data;
                if (!isset($data['text']) && !isset($data['client_id']) && !isset($data['id']))
                    die($this->jsonReturn(true, 'Informe um texto e o ID do cliente e o ID do POST'));
                else {
                    $updateData['Timeline'] = array(
                        'id'        => $data['id'],
                        'user_id'   => $data['user_id'],
                        'client_id' => $data['client_id'],
                        'text'      => $data['text'],
                        'status'    => true
                    );
                    if (!$this->Timeline->save($updateData))
                        die($this->jsonReturn(true, 'Não foi possível atualizar este POST, atualize a página e tente novamente.'));
                    else {
                        $posts = $this->getClientTimeline($data);
                        if ($posts)
                            die($this->jsonReturn(false, 'Registros encontrados para este cliente.', $posts));
                        die($this->jsonReturn(false, 'Nenhum registro informado para esse cliente.'));
                    }
                }
            }
        }

        public function getposts()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST.', array(), 500));
            else {
                $data = $this->request->data;
                if (!isset($data['client_id']))
                    die($this->jsonReturn(true, 'O "ID" do cliente não foi encontrado, por favor, atualize a página ou entre em contato com o desenvolvedor.', array(), 500));
                else {
                    $posts = $this->getClientTimeline($data);
                    if ($posts)
                        die($this->jsonReturn(false, 'Registros encontrados para este cliente.', $posts));
                    die($this->jsonReturn(false, 'Nenhum registro informado para esse cliente.'));
                }
            }
        }

        public function getnotifications()
        {
            $this->layout = false;
            $this->render(false);
            if ($this->request->is('post') && isset($this->request->data['user_id']))
                die($this->jsonReturn(false, 'Retorno de notificações', $this->Notificacao->getNotificationByUser($this->request->data['user_id'])));
            else {
                die;
            }
        }

        public function readnotification()
        {
            $this->layout = false;
            $this->render(false);
            if ($this->request->is('post') && isset($this->request->data['id']) && isset($this->request->data['user_id'])) {

                $data = $this->request->data;
                $this->Notificacao->id = $data['id'];
                $this->Notificacao->saveField('read', true);
                $this->Notificacao->save();

                die($this->jsonReturn(false, 'Retorno de notificações', $this->Notificacao->getNotificationByUser($data['user_id'])));
            } else {
                die;
            }
        }

        public function getusers()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST', array()));
            else {
                $data = $this->request->data;
                die($this->jsonReturn(true, 'Usuários encontrados no sistema.', $this->Usuario->find('list', array('fields' => array('Usuario.id', 'Usuario.nome'), 'conditions' => array('Usuario.id <>' => $data['user_id']))), 200));
            }
        }

        public function delete()
        {
            $this->layout = false;
            $this->render(false);
            if (!$this->request->is('post'))
                die($this->jsonReturn(true, 'Faça uma requisição POST.', array(), 500));
            else {
                $data = $this->request->data;
                if (!isset($data['client_id']) || !isset($data['id']))
                    die($this->jsonReturn(true, 'O "ID" do cliente ou "ID" do post não foi encontrado, por favor, atualize a página ou entre em contato com o desenvolvedor.', array(), 500));
                else {

                    $deleteData = $this->Timeline->find('first',
                        array(
                            'recursive'  => -1,
                            'conditions' => array(
                                'Timeline.id' => $data['id']
                            )
                        )
                    );
                    $deleteData['Timeline']['deleted'] = date('Y-m-d H:i:s');

                    if (!$this->Timeline->save($deleteData))
                        die($this->jsonReturn(true, 'Não foi possível remove este POST, atualize a página e tente novamente.'));
                    else {
                        $posts = $this->getClientTimeline($data);
                        if ($posts)
                            die($this->jsonReturn(false, 'Registros encontrados para este cliente.', $posts));
                        die($this->jsonReturn(false, 'Nenhum registro informado para esse cliente.'));
                    }
                }
            }
        }


        private function jsonReturn($error, $msg, $data = array(), $status = 200)
        {
            return json_encode(
                array(
                    'error'  => $error,
                    'data'   => $data,
                    'msg'    => $msg,
                    'status' => $status
                )
            );
        }

    }
