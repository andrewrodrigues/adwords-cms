<?php

    class CommonComponent extends Component
    {

        var $controller = null;

        public function initialize(Controller $controller)
        {
            $this->controller = $controller;
        }

        /*
         * Verifico que o usuario está no admin
         *
         * @return bollean Resultado da operação
         */
        public function isAdminMode()
        {
            $adminRoute = Configure::read('Routing.prefixes');
            $params = Router::getParams();
            if (isset($params['prefix']) && in_array($params['prefix'], $adminRoute)) {
                return true;
            }
            return false;
        }

        /**
         * @param $model
         * @return bool
         */
        private function getMyCorrectModel($model)
        {
            $black_list = array('.', '..');
            $_dir_ = scandir(APP . 'Plugin');
            foreach ($_dir_ as $dir) {
                if (empty($dir) || in_array($dir, $black_list))
                    continue;
                else {
                    App::import('Model', $dir . '.' . $model);
                    if (class_exists($model)) {
                        return $dir . '.' . $model;
                    }
                }
            }
            throw new NotFoundException($model. __LINE__.":".__FILE__. ', não encontrada');
        }

        /**
         * @param $model
         * @param $id
         * @param $status
         * @return bool
         */
        public function saveStatus($model, $id, $status)
        {
            App::import('Model', $this->getMyCorrectModel($model));
            $this->{$model} = new $model();
            $this->{$model}->id = $id;
            if ($this->{$model}->saveField('status', $status))
                return 1;
            else {
                return 0;
            }
        }

        /**
         * Mêtodo _redirectFilter
         * responsavel por gerenciar os redirects dos filters
         * @return voids
         */
        public function redirectFilter($referer = null)
        {
            if ($referer == null) {
                $referer = $this->controller->referer();
            }

            //custom do redirect para o FilterResults
            if ($referer && stripos($referer, '/fatorcms/')) {
                $this->controller->redirect($referer);
            } else {
                $this->controller->redirect(array('action' => 'index'));
            }
        }

        /**
         * Mêtodo setUrlReferer
         *
         * @return Array
         */
        public function setUrlReferer()
        {
            $referer_tmp = $this->Session->read('referer');
            if ($referer_tmp == "" || $referer_tmp === false) {
                $this->Session->write('referer', $this->referer());
            }
        }
    }
