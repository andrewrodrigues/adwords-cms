<?php

class ImportDbComponent extends Component {

	private function slug($str, $separator = '-'){
		setlocale(LC_ALL, 'pt_BR.ISO-8859-1');
		$str = @iconv('UTF-8', 'ISO-8859-1//IGNORE//TRANSLIT', $str);

		$str = strtolower($str);

	    // Código ASCII das vogais
	    $ascii['a'] = range(224, 230);
	    $ascii['e'] = range(232, 235);
	    $ascii['i'] = range(236, 239);
	    $ascii['o'] = array_merge(range(242, 246), array(240, 248));
	    $ascii['u'] = range(249, 252);

	    // Código ASCII dos outros caracteres
	    $ascii['b'] = array(223);
	    $ascii['c'] = array(231);
	    $ascii['d'] = array(208);
	    $ascii['n'] = array(241);
	    $ascii['y'] = array(253, 255);

	    foreach ($ascii as $key=>$item) {
	        $acentos = '';
	        foreach ($item AS $codigo) $acentos .= chr($codigo);
	        $troca[$key] = '/['.$acentos.']/i';
	    }

	    $str = preg_replace(array_values($troca), array_keys($troca), $str);

	    // Slug?
	    if ($separator) {
	        // Troca tudo que não for letra ou número por um caractere ($slug)
	        $str = preg_replace('/[^a-z0-9]/i', $separator, $str);
	        // Tira os caracteres ($slug) repetidos
	        $str = preg_replace('/' . $separator . '{2,}/i', $separator, $str);
	        $str = trim($str, $separator);
	    }
	    return $str;
	}

	public function importa_atualiza_noticias(){

		App::import('Model', 'FdNoticias.Noticia');
		$NoticiaLalala = new Noticia();     
		$noticias = $NoticiaLalala->find('list', array('fields' => array('Noticia.id_old', 'Noticia.id')));
        // debug($noticias);die;


        $NoticiaPostgre = new Noticia();
        $NoticiaPostgre->setDataSource('postgre');
        $noticias_atuais = $NoticiaPostgre->find('all', array('order' => 'news_id ASC'));
        // debug($noticias_atuais);die;
        
        $NoticiaPostgre->setDataSource('default');
        //$this->NoticiaPostgre->setDataSource('default');

        // debug($this->NoticiaPostgre->getDataSource());die;

        // die;


        $NoticiaLalala->setDataSource('default');
        ini_set('mbstring.substitute_character', "none"); 
        foreach ($noticias_atuais as $key => $value) {
        	$noticia = array();

        	print('<pre>');
        	print_R($noticias[$value['Noticia']['news_id']]);
        	print_R($value);
        	die;
		    
		    if(isset($noticias[$value['Noticia']['news_id']])){
        		$noticia['Noticia']['id'] 			= $noticias[$value['Noticia']['news_id']];
        		$noticia['Noticia']['conteudo'] 	= html_entity_decode($value['Noticia']['news_texto']);

        		if(!$NoticiaLalala->save($noticia)){
					debug($NoticiaLalala->invalidFields());
					die(debug($noticia));
				}

        	}
			
        }


        die;
			
	}

	public function importa_noticias(){

		App::import('Model', 'FdNoticias.Noticia');
        
        $NoticiaPostgre = new Noticia();
     //    $this->NoticiaPostgre->unbindModel(
	    //     array('belongsTo' => array('NoticiaTipo', 'NoticiaCategoria')),
	    //     array('hasAndBelongsToMany' => array('NoticiaTag'))
	    // );
        $NoticiaPostgre->setDataSource('postgre');
        $noticias_atuais = $NoticiaPostgre->find('all', array('order' => 'news_id ASC'));
        // debug($noticias_atuais);die;
        $NoticiaPostgre->setDataSource('default');
        //$this->NoticiaPostgre->setDataSource('default');

        // debug($this->NoticiaPostgre->getDataSource());die;

        $Noticia = new Noticia();        
        ini_set('mbstring.substitute_character', "none"); 
        foreach ($noticias_atuais as $key => $value) {
        	
        	$noticia = array();
        	$Noticia->setDataSource('default');
        	// $Noticia->unbindModel(array('belongsTo' => array('NoticiaTipo', 'NoticiaCategoria')), true);
        	// $Noticia->unbindModel(array('hasAndBelongsToMany' => array('NoticiaTag')), true);
        	// $Noticia->unbindAll();
		    
        	$ja_tem = $Noticia->find('first', array('recursive' => -1, 'conditions' => array('Noticia.id_old' => $value['Noticia']['news_id'])));
        	if(!empty($ja_tem)){
        		$noticia['Noticia']['id'] 		= $ja_tem['Noticia']['id'];
        	}else{
        		$noticia['Noticia']['id'] 		= null;
        	}

			$noticia['Noticia']['id_old'] 				= $value['Noticia']['news_id'];
			$noticia['Noticia']['titulo'] 				= html_entity_decode(strip_tags($value['Noticia']['news_titulo']));
			// $value['Noticia']['news_resumo'] = trim(html_entity_decode(strip_tags($value['Noticia']['news_resumo'])));
			$value['Noticia']['news_resumo'] = trim(strip_tags($value['Noticia']['news_resumo']));
			if($value['Noticia']['news_resumo'] != "" && strlen($value['Noticia']['news_resumo']) > 5){
				$noticia['Noticia']['descricao_resumida'] 	= $value['Noticia']['news_resumo'];
			}else{
				$noticia['Noticia']['descricao_resumida'] = '';
			}
			
			// $noticia['Noticia']['descricao_resumida'] = utf8_encode($noticia['Noticia']['descricao_resumida']);
			// $noticia['Noticia']['descricao_resumida']	= html_entity_decode(iconv("UTF-8", "ISO-8859-1", $value['Noticia']['news_resumo']));
			// $noticia['Noticia']['descricao_resumida'] 	= html_entity_decode(mb_convert_encoding($value['Noticia']['news_resumo'], 'UTF-8', 'UTF-8'));
			$noticia['Noticia']['descricao_resumida'] 	= html_entity_decode(iconv("UTF-8", "ISO-8859-1", $value['Noticia']['news_resumo']));
			
			$noticia['Noticia']['conteudo'] 			= html_entity_decode($value['Noticia']['news_texto']);
			$noticia['Noticia']['data_publicacao'] 		= $value['Noticia']['news_data_hora_postagem'];
			$noticia['Noticia']['created'] 				= $value['Noticia']['data_hora_cadastro'];
			$noticia['Noticia']['modified'] 			= $value['Noticia']['data_hora_ultima_alteracao'];
			$noticia['Noticia']['status'] 				= $value['Noticia']['news_publicada'];
			$noticia['Noticia']['noticia_tipo_id'] 		= 1;
			$noticia['Noticia']['noticia_categoria_id'] = 1;
			$noticia['Noticia']['seo_title'] 			= '';
			$noticia['Noticia']['seo_description'] 		= '';
			$noticia['Noticia']['seo_url'] 				= '';
			$noticia['Noticia']['visivel'] 				= 'GERAL';

			//trata seo_url
			// if (isset($noticia['Noticia']['titulo']) && $noticia['Noticia']['seo_url'] == "") {
				$noticia['Noticia']['seo_url'] = Inflector::slug(strip_tags($value['Noticia']['news_titulo']), '-');
				$noticia['Noticia']['seo_url'] = $this->slug(strip_tags($noticia['Noticia']['titulo']), '-');
			// }

	    	//add a categoria na url
			if(isset($noticia['Noticia']['noticia_categoria_id']) && $noticia['Noticia']['noticia_categoria_id'] != ""){
				App::import('Model', 'FdNoticias.NoticiaCategoria');
				$this->NoticiaCategoria = new NoticiaCategoria();
				$noticia_categoria = $this->NoticiaCategoria->find('first', array('recursive' => -1, 'conditions' => array('NoticiaCategoria.id' => $noticia['Noticia']['noticia_categoria_id']), 'fields' => array('NoticiaCategoria.seo_url')));
				if(!empty($noticia_categoria)){
					if($noticia['Noticia']['id'] == null){
						$noticia['Noticia']['seo_url'] = $noticia_categoria['NoticiaCategoria']['seo_url'].'/'.$noticia['Noticia']['seo_url'];
					}else{
						if(stripos($noticia['Noticia']['seo_url'], '/')){
							$explode = array_reverse(explode('/', $noticia['Noticia']['seo_url']));
							$noticia['Noticia']['seo_url'] = $explode[0];
						}
						$noticia['Noticia']['seo_url'] = $noticia_categoria['NoticiaCategoria']['seo_url'].'/'.$noticia['Noticia']['seo_url'];
					}
				}
			}

			App::import('Model', 'FdRotas.Rota');
	    	$this->Rota = new Rota();
	    	// begin provisório
	    	if(is_null($noticia['Noticia']['id'])){
		    	$rota = $this->Rota->find('all', array('conditions' => array('seo_url' => $noticia['Noticia']['seo_url'])));
		    	if(!empty($rota)){
		    		$noticia['Noticia']['seo_url'] = $noticia['Noticia']['seo_url'] . '-' . time();
		    	}
		    }else{
		    	$rota = $this->Rota->find('all', array('conditions' => array('row_id <>' => $noticia['Noticia']['id'], 'seo_url' => $noticia['Noticia']['seo_url'])));
		    	if(!empty($rota)){
		    		$noticia['Noticia']['seo_url'] = $noticia['Noticia']['seo_url'] . '-' . $noticia['Noticia']['id'];
		    	}
		    }
	    	// end provisório

	  //   	print('<pre>');
			// print_r($noticia);die;
			// continue;
			if(!$Noticia->save($noticia)){
				debug($Noticia->invalidFields());
				die(debug($noticia));
			}
        }


        die;
			
	}

	public function importa_eventos(){
		App::import('Model', 'FdEventos.Evento');
        $EventoPostgre = new Evento();
        $EventoPostgre->setDataSource('postgre');


        App::import('Model', 'FdEventos.EventoData');
        $EventoDataPostgre = new EventoData();
        $EventoDataPostgre->setDataSource('postgre');


        $eventos = $EventoPostgre->find('all', array('order' => 'even_id ASC'));

        // debug($eventos);die;
        
        $Evento = new Evento();
        foreach ($eventos as $key => $value) {
        	$evento = array();

        	$ja_tem = $Evento->find('first', array('recursive' => -1, 'conditions' => array('Evento.id_old' => $value['Evento']['even_id'])));
        	if(!empty($ja_tem)){
        		$evento['Evento']['id'] 		= $ja_tem['Evento']['id'];
        	}else{
        		$evento['Evento']['id'] 		= null;
        	}


        	$evento['Evento']['id_old'] 				= $value['Evento']['even_id'];
			$evento['Evento']['nome'] 					= html_entity_decode(strip_tags($value['Evento']['even_titulo']));
			$evento['Evento']['created'] 				= $value['Evento']['data_hora_cadastro'];
			$evento['Evento']['modified'] 				= ($value['Evento']['data_hora_ultima_alteracao'] != "") ? $value['Evento']['data_hora_ultima_alteracao'] : $value['Evento']['data_hora_cadastro'];
			$evento['Evento']['status'] 				= $value['Evento']['even_ativo'];
			$evento['Evento']['seo_title'] 				= '';
			$evento['Evento']['seo_description'] 		= '';
			$evento['Evento']['seo_url'] 				= '';

			$data = $EventoDataPostgre->find('first', array('conditions' => array('EventoData.even_id' => $evento['Evento']['id_old'])));
			if(!empty($data)){
				// $evento['Evento']['data'] 				= substr($data['EventoData']['evda_data_hora_ini'], 0, stripos($data['EventoData']['evda_data_hora_ini'], ' '));
				$evento['Evento']['data_inicio'] 				= substr($data['EventoData']['evda_data_hora_ini'], 0, stripos($data['EventoData']['evda_data_hora_ini'], ' '));
				$evento['Evento']['data_fim'] 				= substr($data['EventoData']['evda_data_hora_fim'], 0, stripos($data['EventoData']['evda_data_hora_fim'], ' '));
			}

			$evento['Evento']['seo_url'] = 'evento/'.$this->slug(strip_tags($evento['Evento']['nome']), '-');


			App::import('Model', 'FdRotas.Rota');
	    	$this->Rota = new Rota();
	    	// begin provisório
	    	if(is_null($evento['Evento']['id'])){
	    		$rota = $this->Rota->find('all', array('conditions' => array('seo_url' => $evento['Evento']['seo_url'])));
		    	if(!empty($rota) && is_null($evento['Evento']['id'])){
		    		$evento['Evento']['seo_url'] = $evento['Evento']['seo_url'] . '-' . time();
		    	}
	    	}else{
	    		$rota = $this->Rota->find('all', array('conditions' => array('row_id <>' => $evento['Evento']['id'], 'seo_url' => $evento['Evento']['seo_url'])));
		    	if(!empty($rota)){
		    		$evento['Evento']['seo_url'] = $evento['Evento']['seo_url'] . '-' . $evento['Evento']['id'];
		    	}
	    	}
	    	
	    	// end provisório

			if(!$Evento->save($evento)){
				debug($Evento->invalidFields());
				die(debug($evento));
			}

        }




        die;
	}

	public function importa_noticias_urls(){
		App::import('Model', 'FdNoticias.Noticia');
        $Noticia = new Noticia();

        App::import('Model', 'FdRotas.Redirecionamento');
        $Redirecionamento = new Redirecionamento();

        $noticias = $Noticia->find('all');

        foreach ($noticias as $key => $noticia) {
        	$u = $Redirecionamento->find('first', array('conditions' => array('Redirecionamento.url_antiga' => 'index.php?noticia=' . $noticia['Noticia']['id_old'])));
	        if(!empty($u)){
	            $url['Redirecionamento']['id']           = $u['Redirecionamento']['id'];
	        }else{
	            $url['Redirecionamento']['id']           = null;
	        }
	        $url['Redirecionamento']['url_antiga']   = 'index.php?noticia=' . $noticia['Noticia']['id_old'];
	        $url['Redirecionamento']['url_nova']     = $noticia['Noticia']['seo_url'];
	        $url['Redirecionamento']['tipo']         = 301;
	        if($url['Redirecionamento']['url_antiga'] != $url['Redirecionamento']['url_nova']){
	            $Redirecionamento->save($url);
	        }
        }

        die;	
	}

	public function importa_imagens_noticias(){

		App::import('Model', 'FdNoticias.Noticia');
		$Noticia = new Noticia();
		$noticias = $Noticia->find('all', array('conditions' => array('Noticia.thumb' => null), 'recursive' => -1));
		// debug($noticias);die;

		$path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'noticia' . DS . 'thumb';

		foreach ($noticias as $key => $noticia) {
			
			$path_thumb_atual = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'importacoes' . DS . 'banners_noticias';

	        $path_thumb_novo = $path . DS . $noticia['Noticia']['id'];

	        

	        if(file_exists($path_thumb_atual .  DS . $noticia['Noticia']['id_old'].'.jpg')){
	        	if(!is_dir($path_thumb_novo)){
		            mkdir($path_thumb_novo);
		        }

	            copy($path_thumb_atual .  DS . $noticia['Noticia']['id_old'].'.jpg', $path_thumb_novo .  DS . $noticia['Noticia']['id_old'].'.jpg');
	        

	            //load lib
		        include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

		        //renomeio o arquivo, para deixar com o mesmo nome do produto
		        $pre_new_name = strtolower($this->slug($noticia['Noticia']['titulo'], '-'));
		        $pre_new_name = str_replace("_", "-", $pre_new_name);
		        $pre_old_name = $noticia['Noticia']['id_old'].'.jpg';

		        $new_name = $pre_new_name;
		        $old_name = $pre_old_name;

		        //arquivo existe?
		        if(file_exists($path_thumb_novo . DS . $old_name)){
		            $file = $path_thumb_novo . DS . $old_name;
		            $infos = pathinfo($file);
		            $size = filesize($file);
		            $new_file = $path_thumb_novo . DS . $new_name . '.' . $infos['extension'];

		            //renomeou o thumb?
		            if(rename($file, $new_file)){

		                //do update na base
		                $Noticia->save(array(
		                                // 'thumb' => $new_name . '.' . $infos['extension'],
		                                'thumb_type'  => 'image/jpeg',
		                                'thumb_size'  => $size,
		                                'thumb_dir'   => $noticia['Noticia']['id'],
		                                'thumb' => $new_name . '.' . $infos['extension'],
		                                'id'	=> $noticia['Noticia']['id']
		                            ), 
		                            array('callbacks' => false, 'validate' => false)
		                        );
		                // $this->query('UPDATE produtos SET thumb = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

		                $caminho_novo = $path_thumb_novo;

		                $outFile    = $caminho_novo.DS.'700x400-'.$pre_new_name . '.' . $infos['extension'];
						WideImage::load($new_file)->resize(700)->crop('center', 'center', 700, 400)->saveToFile($outFile);

						$outFile    = $caminho_novo.DS.'700x258-'.$pre_new_name . '.' . $infos['extension'];
						WideImage::load($new_file)->resize(700)->crop('center', 'center', 700, 258)->saveToFile($outFile);

						$outFile    = $caminho_novo.DS.'330x150-'.$pre_new_name . '.' . $infos['extension'];
						WideImage::load($new_file)->resize(330)->crop('center', 'center', 330, 150)->saveToFile($outFile);

						$outFile    = $caminho_novo.DS.'250x250-'.$pre_new_name . '.' . $infos['extension'];
	                    WideImage::load($new_file)->resize(250)->crop('center', 'center', 250, 250)->saveToFile($outFile);
		            }
		        }
	        }
	        
		}

		die;
	}

	public function importa_professores(){
		App::import('Model', 'SigaProfessor');
        $this->SigaProfessor = new SigaProfessor();
        $professores = $this->SigaProfessor->find('all');
        $retorno = array(); 
        // debug($professores);die;
        foreach ($professores as $key => $prof) {
        	$curso_id = $prof['SigaProfessor']['curs_id'];
        	$nens_id = $prof['SigaProfessor']['nens_id'];
            foreach ($prof['SigaProfessor']['informacao_aprovada']['corpo_docente'] as $a => $corpo) {
            	$habilitacao_id = $a;
                foreach ($corpo['coordenador'] as $y => $coordenador) {
                    foreach ($coordenador as $x =>  $value) {
                        $retorno[$x]['Professor']['coordenador_tipo'] = $y;
                        $retorno[$x]['Professor']['siga_codigo_id'] = $x;
                        $retorno[$x]['Professor']['nome'] = $value['nome'];
                        $retorno[$x]['Professor']['nome'] = $value['nome'];
                        $retorno[$x]['Professor']['email'] = $value['email'];
                        $retorno[$x]['Professor']['titulacao'] = $value['titulacao'];
                        $retorno[$x]['Professor']['status'] = $value['ativo'];

						$retorno[$x]['Professor']['curso_id'] = $curso_id;
						$retorno[$x]['Professor']['nens_id'] = $nens_id;
						$retorno[$x]['Professor']['habilitacao_id'] = $habilitacao_id;
                    }
                }
                # code...
            }
        }

        // debug($retorno);die;

        App::import('Model', 'FdCursos.Professor');
        $this->Professor = new Professor();

        App::import('Model', 'FdCursos.CursoCampus');
        $this->CursoCampus = new CursoCampus();

        foreach ($retorno as $key => $value) {
        	
        	$ja_tem = $this->Professor->find('first', array('recursive' => -1, 'conditions' => array('Professor.siga_codigo_id' => $value['Professor']['siga_codigo_id'])));
        	if(!empty($ja_tem)){
        		$value['Professor']['id'] = $ja_tem['Professor']['id'];
        	}else{
        		$value['Professor']['id'] = null;
        	}

        	if($this->Professor->save($value)){
        		$professor_id = $this->Professor->id;
        		$curso_campi = $this->CursoCampus->find('first', array(
        															'recursive' => -1, 
        															'conditions' => array(
        																	'CursoCampus.siga_curso_id' => $value['Professor']['curso_id'],
        																	'CursoCampus.siga_habilitacao_id' => $value['Professor']['habilitacao_id']
        															)
        														)
        													);
        		if(!empty($curso_campi)){
        			$coord = array();
        			if(is_array($curso_campi['CursoCampus']['coordenadores']) && count($curso_campi['CursoCampus']['coordenadores']) > 0){
        				foreach ($curso_campi['CursoCampus']['coordenadores'] as $key => $v) {
        					$coord[$v] = $v;
        				}
        				$coord[$professor_id] = $professor_id;
        			}else{
        				$coord[$professor_id] = $professor_id;
        			}

        			$this->CursoCampus->save(array('coordenadores' => array_values($coord), 'id' => $curso_campi['CursoCampus']['id']), false);
        		}

        	}

        }

        die;
	}

	public function importa_busca(){

		App::import('Model', 'FdCursos.Curso');
		$Curso = new Curso();

		$cursos = $Curso->find('all', array(
											'recursive' => -1, 
											'fields' => array('id', 'nome', 'seo_url', 'descricao', 'informacoes_legais', 'objetivo', 'estrutura', 'mercado_trabalho', 'horario', 'informacoes_adicionais', 'projetos', 'seo_description', 'seo_keywords')
										));
		foreach ($cursos as $key => $curso) {
			$curso['Curso']['modified'] = false;
			$Curso->save($curso);
		}

		//noticia

		App::import('Model', 'FdNoticias.Noticia');
		$Noticia = new Noticia();

		$cursos = $Noticia->find('all', array(
											'recursive' => -1, 
											'fields' => array('id', 'titulo', 'data_publicacao', 'seo_url', 'conteudo', 'seo_description', 'seo_keywords')
										));
		foreach ($cursos as $key => $curso) {
			$curso['Noticia']['modified'] = false;
			$Noticia->save($curso);
		}


		//Evento

		App::import('Model', 'FdEventos.Evento');
		$Evento = new Evento();

		$cursos = $Evento->find('all', array(
											'recursive' => -1, 
											'fields' => array('id', 'nome', 'data', 'seo_url', 'conteudo', 'seo_description', 'seo_keywords')
										));
		foreach ($cursos as $key => $curso) {
			$curso['Evento']['modified'] = false;
			$Noticia->save($curso);
		}


		//Pagina

		App::import('Model', 'FdPaginas.Pagina');
		$Pagina = new Pagina();

		$cursos = $Pagina->find('all', array(
											'recursive' => -1, 
											'fields' => array('id', 'nome', 'seo_url', 'conteudo', 'seo_description', 'seo_keywords')
										));
		foreach ($cursos as $key => $curso) {
			$curso['Pagina']['modified'] = false;
			$Noticia->save($curso);
		}

	}

	public function importa_instituicoes(){
		$root = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'importacoes' . DS . 'arquivos' . DS;
		$file = $root.'instituicoes.csv';

		App::import('Model', 'FdInstituicoes.Instituicao');
		$this->Instituicao = new Instituicao();

		App::import('Model', 'FdInstituicoes.InstituicaoCurso');
		$this->InstituicaoCurso = new InstituicaoCurso();

		$handle = fopen($file, "r");

        $first = true;
        $cont = 1;
        $ultimo_id = 0;
        $ultimo_nome = '';
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            if($first){
                $first = false;
                continue;
            }else{
            	if($data[0] != ""){
            		
	            	$nome 			= $data[6];
	            	$pais 			= $data[5];
	            	// $cidade 		= $data[0];
	            	$nome_curso_br 	= $data[0];
	            	$nome_curso_es 	= $data[2];
	            	$nome_curso_en 	= $data[1];
	            	// $url_do_curso 	= $data[0];
	            	$endereco 		= $data[3];
	            	$zipcode 		= $data[4];
	            	// $latitude 		= $data[0];
	            	// $longitude 		= $data[0];
	            	// $apresentacao 	= $data[0];
	            	$url_site 		= $data[7];
	            	$status 		= true;

	            	$inst = array();
	            	
	            	$inst['Instituicao']['nome'] 			= utf8_encode($nome);
	            	$inst['Instituicao']['id'] 				= ($ultimo_nome != $inst['Instituicao']['nome']) ? null : $ultimo_id;
	            	$inst['Instituicao']['pais'] 			= utf8_encode($pais);	            	
	            	$inst['Instituicao']['endereco'] 		= utf8_encode($endereco);
	            	$inst['Instituicao']['zipcode']			= utf8_encode($zipcode);
	            	$inst['Instituicao']['url_site'] 		= $url_site;
	            	$inst['Instituicao']['status'] 			= $status;

	            	if($this->Instituicao->save($inst)){

	            		$inst_curso = array();
	            		$inst_curso['InstituicaoCurso']['id']				= null;
	            		$inst_curso['InstituicaoCurso']['instituicao_id']	= $this->Instituicao->id;
	            		$inst_curso['InstituicaoCurso']['nome_curso_br'] 	= utf8_encode($nome_curso_br);
		            	$inst_curso['InstituicaoCurso']['nome_curso_es'] 	= utf8_encode($nome_curso_es);
		            	$inst_curso['InstituicaoCurso']['nome_curso_en'] 	= utf8_encode($nome_curso_en);
		            	$inst_curso['InstituicaoCurso']['status']			= true; 

		            	if(!$this->InstituicaoCurso->save($inst_curso)){
		            		die(debug($inst_curso));
		            	}
	            	}	

	            	$ultimo_id = $this->Instituicao->id;
	            	$ultimo_nome = $inst['Instituicao']['nome'];     	
            	}
            }
        }

        die;
	}


	public function importa_geo_instituicoes(){
		$root = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'importacoes' . DS . 'arquivos' . DS;
		$file = $root.'instituicoes.csv';

		App::import('Model', 'FdInstituicoes.Instituicao');
		$this->Instituicao = new Instituicao();

		$instituicoes = $this->Instituicao->find('all', array('group' => 'Instituicao.endereco'));

		foreach ($instituicoes as $key => $instituicao) {
			
			if($instituicao['Instituicao']['endereco'] != ""){
				$address = $instituicao['Instituicao']['endereco'];
				$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
		    	$output = json_decode($geocode);

		    	if($output->results[0]->geometry->location->lat != ""){
		    		$lat = $output->results[0]->geometry->location->lat;
		    		$lng = $output->results[0]->geometry->location->lng;

		    		$query = 'UPDATE instituicoes SET latitude="' . $lat . '", longitude="' . $lng . '" WHERE endereco = "' . $address . '"';
		    		// var_dump($query);
		 //    		print('<hr />');
		    		$this->Instituicao->query($query);
		    	}
		    	
			}
			

		}


        die();
	}

	public function importa_infos_pos(){

		App::import('Model', 'FdCursos.Curso');

		$Curso = new Curso();

		$CursoPos = new Curso();
        $CursoPos->setDataSource('pos');
        $cursos_pos = $CursoPos->find('all');

        if(count($cursos_pos)){
        	foreach ($cursos_pos as $key => $curso_pos) {
        		
        		$curso = $Curso->find('first', array('recursive' => -1, 'conditions' => array('Curso.id' => $curso_pos['Curso']['id_portal'])));

        		if(!empty($curso)){

        			$save = array();
        			$save['id'] 					= $curso['Curso']['id']; 
        			$save['pos_periodo_realizacao'] = $curso_pos['Curso']['periodo_realizacao']; 
        			$save['pos_carga_horaria'] 		= $curso_pos['Curso']['carga_horaria'];
        			$save['pos_publico_alvo'] 		= $curso_pos['Curso']['publico_alvo'];
        			$save['pos_objetivo'] 			= $curso_pos['Curso']['objetivo'];

        			$save['pos_selecao'] 			= $curso_pos['Curso']['selecao'];
        			$save['pos_documentacao'] 		= $curso_pos['Curso']['documentacao'];
        			$Curso->save($save, false);
        		}

        	}
        }

        die('fim');
        debug($cursos_pos);die;
	}

	public function importa_infos_pos_horarios_e_valores(){
		App::import('Model', 'FdCursos.Curso');
		App::import('Model', 'FdCursos.CursoCampus');

		$Curso = new Curso();
		$CursoCampus = new CursoCampus();

		$CursoPos = new Curso();
        $CursoPos->setDataSource('pos');

        $CursoCampusPos = new CursoCampus();
        $CursoCampusPos->setDataSource('pos');

        $cursos_pos = $CursoPos->find('all', array('recursive' => -1));

        if(count($cursos_pos)){
        	foreach ($cursos_pos as $key => $curso_pos) {

        		$curso_campus_pos = $CursoCampusPos->find('all', array('recursive' => -1, 'conditions' => array('curso_id' => $curso_pos['Curso']['id'])));
        		
        		if(count($curso_campus_pos)){
        			foreach ($curso_campus_pos as $key => $value) {

        				$curso_campus = $CursoCampus->find('first', array('recursive' => -1, 'conditions' => array('curso_id' => $curso_pos['Curso']['id_portal'], 'campus_id' => $value['CursoCampus']['campus_id'])));
        				
        				if(!empty($curso_campus)){

        					$array = array();
        					$array['id']							= $curso_campus['CursoCampus']['id'];
        					$array['pos_parcelas_numero']			= $value['CursoCampus']['parcelas_numero'];
        					$array['pos_parcela_valor']				= $value['CursoCampus']['parcela_valor'];
        					$array['pos_valor_total']				= $value['CursoCampus']['valor_total'];
        					$array['pos_horario']					= $value['CursoCampus']['horario'];
        					$array['pos_informacoes_adicionais']	= $value['CursoCampus']['informacoes_adicionais'];

        					$CursoCampus->save($array, false);
        				}
        			}
        		}

        	}
       	}

       	die;
	}

}